--
-- IIF
--
SELECT 
      IIF([L�ndername]='Deutschland','Germany',[L�ndername]) as Country
FROM [schulungsdatenbank].[dbo].[tbl_Raum]

--
-- CASE
--
SELECT 
	Zeit_ID,
	DATEPART(quarter,Zeit_ID) as Quartal,
    CASE DATEPART(quarter,Zeit_ID)
        WHEN 1 THEN '1. Quartal'
        WHEN 2 THEN '2. Quartal'
        WHEN 3 THEN '3. Quartal'
        WHEN 4 THEN '4. Quartal'
        ELSE 'Unbekannt'
    END AS AbteilungsName,
	MONTH(Zeit_ID) as Monatsnummer,
	CASE
		WHEN MONTH(Zeit_ID) IN (1, 2) THEN 'Winter'
		WHEN MONTH(Zeit_ID) IN (3, 4, 5) THEN 'Fr�hling'
		WHEN MONTH(Zeit_ID) IN (6, 7, 8) THEN 'Sommer'
		WHEN MONTH(Zeit_ID) IN (9, 10, 11) THEN 'Herbst'
		WHEN MONTH(Zeit_ID) = 12 THEN 'Winter'
	END As Quartal
	FROM
    tbl_Geh�lter

--
-- CHOOSE
--
SELECT 
	Zeit_ID,
	DATEPART(quarter,Zeit_ID) as Quartal,
	CHOOSE ( DATEPART(quarter,Zeit_ID) , 
			'1. Quartal',   /* Wenn Index 1 */
			'2. Quartal',   /* Wenn Index 2 */ 
			'3. Quartal',   /* Wenn Index 3 */
			'4. Quartal'	/* Wenn Index 4 */
			) as Quartal,
	MONTH(Zeit_ID) as Monatsnummer,
	CHOOSE(MONTH(Zeit_ID),
			'Winter','Winter',						/* Wenn `Index`1,2 */
			'Fr�hling','Fr�hling','Fr�hling',		/* Wenn `Index`3,4,5 */
			'Sommer','Sommer','Sommer', 			/* Wenn `Index`6,7,8 */
			'Herbst','Herbst','Herbst','Herbst',	/* Wenn `Index`9,10,11 */
			'Winter'								/* Wenn `Index`12  */
			) as Jahreszeit
FROM
      tbl_Geh�lter
