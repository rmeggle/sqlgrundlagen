--
-- Beispiel einer Skalarwertfunktion im WHERE nach Datum
--
SELECT
m.Mitarbeiter,
g.Monatsgehalt
FROM 
tbl_Mitarbeiter m
INNER JOIN tbl_Geh�lter g ON g.MitarbeiterID=m.Mitarbeiter_ID
WHERE YEAR(g.Zeit_ID)=2023

-- 
-- Beispiel einer Skalarwertfunktion im Select  nach Datum
--
SELECT
m.Mitarbeiter,
MONTH(g.Zeit_ID) as Monat
FROM 
tbl_Mitarbeiter m
INNER JOIN tbl_Geh�lter g ON g.MitarbeiterID=m.Mitarbeiter_ID
WHERE YEAR(g.Zeit_ID)=2023
GROUP BY m.Mitarbeiter,MONTH(g.Zeit_ID)
ORDER BY Monat

--
-- Mathematische Funktionen (kleiner Auszug)
--
SELECT Monatsgehalt,
       ROUND(Monatsgehalt, 0) AS Rounded,
       FLOOR(Monatsgehalt) AS Floor,
       CEILING(Monatsgehalt) AS Ceiling,
       Monatsgehalt * RAND() AS Randomized,
	   ROUND( (Monatsgehalt * RAND()),0) AS RandomizedRounded
FROM tbl_Geh�lter;