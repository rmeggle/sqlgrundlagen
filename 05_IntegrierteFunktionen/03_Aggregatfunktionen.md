# Aggregatsfunktionen

Die meisten bisher betrachteten Abfragen behandeln *jede Zeile einzeln* und nutzen eine WHERE-Klausel zum Filtern. Jede resultierende Zeile spiegelt eine Zeile aus dem ursprünglichen Datensatz wider.

SQL Server bietet eine Vielzahl an Aggregatfunktionen. Wir konzentrieren uns in diesem Abschnitt auf die gängigsten Funktionen, darunter SUM, MIN, MAX, AVG und COUNT:

| Funktionsname | Syntax             | Beschreibung |
|---------------|--------------------|--------------|
| SUM           | `SUM(Ausdruck)`    | Summiert alle numerischen Werte, die nicht NULL sind, in einer Spalte. |
| AVG           | `AVG(Ausdruck)`    | Bildet den Durchschnitt aller numerischen Werte, die nicht NULL sind, in einer Spalte (Summe geteilt durch Anzahl). |
| MIN           | `MIN(Ausdruck)`    | Ermittelt die kleinste Zahl, das früheste Datum/die früheste Uhrzeit oder die zuerst vorkommende Zeichenfolge in einer Spalte, basierend auf den Sortierregeln. |
| MAX           | `MAX(Ausdruck)`    | Ermittelt die größte Zahl, das späteste Datum/die späteste Uhrzeit oder die zuletzt vorkommende Zeichenfolge in einer Spalte, basierend auf den Sortierregeln. |
| COUNT oder COUNT_BIG | `COUNT(*)` oder `COUNT(Ausdruck)` | Zählt alle Zeilen, einschließlich derer mit NULL-Werten, wenn (*) verwendet wird. Wenn eine Spalte als Ausdruck angegeben wird, wird die Anzahl der Zeilen dieser Spalte, die ungleich NULL sind, zurückgegeben. `COUNT` gibt einen Wert vom Typ `int` zurück, `COUNT_BIG` einen Wert vom Typ `big_int`. |

Diese Aggregatfunktionen sind grundlegend für die Datenanalyse in SQL und ermöglichen es, Zusammenfassungen und statistische Informationen aus Daten zu extrahieren.

Beim Einsatz von Aggregatfunktionen sollten Sie Folgendes beachten:

- Aggregatfunktionen liefern einen einzigen (skalaren) Wert und sind fast überall in SELECT-Anweisungen einsetzbar, wo einzelne Werte benötigt werden. Sie finden beispielsweise Verwendung in SELECT-, HAVING- und ORDER BY-Klauseln, jedoch nicht in WHERE-Klauseln.
- Aggregatfunktionen ignorieren in der Regel NULL-Werte, die einzige Ausnahme bildet die Funktion COUNT(*).
- Aggregatfunktionen in einer SELECT-Liste haben standardmäßig keinen Spaltenüberschrift, es sei denn, Sie vergeben mithilfe von AS einen Alias.
- Werden Aggregatfunktionen in einer SELECT-Liste eingesetzt, beziehen sie sich auf alle Zeilen, die der SELECT-Operation zugeführt werden. Fehlt eine GROUP BY-Klausel, werden alle Zeilen zusammengefasst, die eventuelle Filter in der WHERE-Klausel passieren. Mehr zu GROUP BY erfahren Sie in der nächsten Lerneinheit.
- Ohne den Einsatz von GROUP BY sollten Sie keine Aggregatfunktionen mit Spalten kombinieren, die nicht Teil der Aggregatfunktionen in derselben SELECT-Liste sind.

Beispiel:
```sql
SELECT 
	Mitarbeiter,
    SUM([Umsatz]) AS GesamtUmsatz,
    AVG([Umsatz]) AS DurchschnittsUmsatz,
    MIN([Umsatz]) AS MinimalerUmsatz,
    MAX([Umsatz]) AS MaximalerUmsatz,
    COUNT(*) AS AnzahlVerkäufe
FROM 
    [dbo].[tbl_Verkäufe]
INNER JOIN [dbo].[tbl_Mitarbeiter] on [dbo].[tbl_Mitarbeiter].Mitarbeiter_ID=[dbo].[tbl_Verkäufe].Mitarbeiter_ID
GROUP BY 
    Mitarbeiter
ORDER BY
	Mitarbeiter
```