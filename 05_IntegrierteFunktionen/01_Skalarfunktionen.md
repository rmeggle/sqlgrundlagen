Skalarfunktionen in SQL sind Funktionen, die einen einzigen Wert zurückgeben und in der Regel auf eine einzelne Datenzeile angewendet werden. Diese Funktionen können eine unterschiedliche Anzahl von Eingabewerten annehmen, von keinem Eingabewert (wie `GETDATE()`) bis zu einem oder mehreren Werten (wie `UPPER()` oder `ROUND()`). Da Skalarfunktionen immer nur einen einzigen Wert zurückgeben, können sie überall dort eingesetzt werden, wo ein einzelner Wert erforderlich ist. Am häufigsten werden sie in `SELECT`-Klauseln und in den Prädikaten von `WHERE`-Klauseln verwendet, können aber auch in der `SET`-Klausel einer `UPDATE`-Anweisung eingesetzt werden.

**Kategorien von integrierten Skalarfunktionen:**
- Konfigurationsfunktionen (Transact-SQL)
- Konvertierungsfunktionen
- Cursorfunktionen
- Datums- und Uhrzeitfunktionen
- Mathematische Funktionen
- Metadatenfunktionen
- Sicherheitsfunktionen
- Zeichenfolgenfunktionen
- Systemfunktionen
- Statistische Systemfunktionen
- Text- und Bildfunktionen

**Überlegungen zur Verwendung von Skalarfunktionen:**
- **Determinismus**: Eine Funktion gilt als deterministisch, wenn sie bei jedem Aufruf für denselben Eingabe- und Datenbankzustand denselben Wert zurückgibt. Zum Beispiel gibt `ROUND(1.1, 0)` immer 1.0 zurück. Nicht deterministische Funktionen wie `GETDATE()`, die das aktuelle Datum und die aktuelle Uhrzeit zurückgeben, können nicht indiziert werden und können die Leistungsfähigkeit des Abfrageprozessors beeinträchtigen.
- **Sortierung**: Bei der Verwendung von Funktionen, die Zeichendaten bearbeiten, muss die verwendete Sortierung berücksichtigt werden. Einige Funktionen verwenden die Sortierung des Eingabewerts, während andere die Sortierung der Datenbank verwenden, wenn keine Eingabesortierung angegeben wird.

**Beispiel für die Anwendung von Skalarfunktionen:**
In Ihrem Beispiel nutzen Sie Datums- und Uhrzeitfunktionen, um Daten aus den Tabellen `tbl_Mitarbeiter` und `tbl_Gehälter` für das Jahr 2023 zu filtern.

```sql
SELECT
    m.Mitarbeiter,
    g.Monatsgehalt
FROM 
    tbl_Mitarbeiter m
INNER JOIN 
    tbl_Gehälter g ON g.MitarbeiterID = m.Mitarbeiter_ID
WHERE 
    YEAR(g.Zeit_ID) = 2023;
```

In dieser Abfrage:
- Sie verknüpfen `tbl_Mitarbeiter` und `tbl_Gehälter` mit einem `INNER JOIN`.
- Sie verwenden die Funktion `YEAR()` in der `WHERE`-Klausel, um nur die Gehälter für das Jahr 2023 zu filtern.
- `YEAR(g.Zeit_ID) = 2023` stellt sicher, dass nur Zeilen ausgewählt werden, deren `Zeit_ID` im Jahr 2023 liegt.

# Skalarfunktionen für Datum und Uhrzeit

Die Skalarfunktionen für Datum und Uhrzeit in SQL bieten verschiedene Möglichkeiten, um Datums- und Zeitwerte zu bearbeiten, zu konvertieren und Informationen daraus abzuleiten. Hier ist eine tabellarische Übersicht über einige der gängigsten Datums- und Zeitfunktionen in SQL:

| Funktion            | Beschreibung                                                        |
|---------------------|---------------------------------------------------------------------|
| `GETDATE()`         | Gibt das aktuelle Datum und die aktuelle Uhrzeit zurück.             |
| `GETUTCDATE()`      | Gibt das aktuelle Datum und die aktuelle Uhrzeit in UTC zurück.      |
| `DATEADD()`         | Fügt einen spezifizierten Zeitwert zu einem Datum hinzu.             |
| `DATEDIFF()`        | Berechnet den Unterschied zwischen zwei Daten.                       |
| `DATEPART()`        | Gibt den spezifizierten Teil eines Datums zurück (z.B. Jahr, Monat). |
| `DATENAME()`        | Gibt den spezifizierten Teil eines Datums als Text zurück.           |
| `DAY()`             | Gibt den Tag des Monats aus einem Datumswert zurück.                 |
| `MONTH()`           | Gibt den Monat aus einem Datumswert zurück.                          |
| `YEAR()`            | Gibt das Jahr aus einem Datumswert zurück.                           |
| `CONVERT()`         | Konvertiert einen Datentyp in einen anderen. Oft verwendet, um Datums- und Zeitformate zu ändern. |
| `CAST()`            | Ähnlich wie `CONVERT`, zum Konvertieren von Datentypen.              |
| `FORMAT()`          | Formatiert Datum und Zeit in ein gewünschtes Format.                 |
| `SYSDATETIME()`     | Gibt das Datum und die Uhrzeit des Systems zurück (höhere Genauigkeit als `GETDATE()`). |
| `CURRENT_TIMESTAMP` | Gibt das aktuelle Datum und die aktuelle Uhrzeit zurück (standardmäßig äquivalent zu `GETDATE()`). |
| `ISDATE()`          | Überprüft, ob ein Ausdruck ein gültiges Datum ist.                   |

Diese Funktionen werden häufig in SQL-Abfragen verwendet, um mit Datums- und Zeitwerten zu arbeiten, sie zu filtern, zu vergleichen oder in bestimmte Formate zu konvertieren. Die genaue Syntax und Verfügbarkeit dieser Funktionen können je nach verwendetem SQL-Dialekt und Datenbankmanagementsystem variieren.

# Für Benutzer

In SQL gibt es auch eine Reihe von Skalarfunktionen, die sich auf Benutzerinformationen und -berechtigungen beziehen. Diese Funktionen sind besonders nützlich, um Informationen über den aktuellen Benutzer, die Rollen und die Zugriffsrechte zu erhalten. Hier ist eine tabellarische Übersicht einiger gängiger Benutzerfunktionen in SQL:

| Funktion               | Beschreibung                                                                 |
|------------------------|------------------------------------------------------------------------------|
| `CURRENT_USER`         | Gibt den Namen des aktuellen Benutzers zurück.                               |
| `SESSION_USER`         | Gibt den Benutzernamen der aktuellen Sitzung zurück.                         |
| `SYSTEM_USER`          | Gibt den Login-Namen des Benutzers zurück, der die aktuelle Sitzung gestartet hat. |
| `USER_NAME()`          | Gibt den Benutzernamen basierend auf der Benutzer-ID zurück.                 |
| `USER_ID()`            | Gibt die Benutzer-ID des angegebenen Benutzernamens zurück.                  |
| `IS_MEMBER()`          | Überprüft, ob der aktuelle Benutzer Mitglied einer bestimmten Windows- oder SQL-Server-Rolle ist. |
| `IS_ROLEMEMBER()`      | Überprüft, ob der aktuelle Benutzer oder eine angegebene Benutzergruppe Mitglied einer bestimmten Rolle ist. |
| `HAS_PERMS_BY_NAME()`  | Gibt an, ob der Benutzer die spezifizierten Berechtigungen für ein gegebenes Objekt besitzt. |
| `SUSER_SNAME()`        | Gibt den Benutzernamen des SQL Server-Benutzers zurück.                       |
| `SUSER_NAME()`         | Gibt den Login-Namen des Benutzers basierend auf der Benutzer-ID zurück.     |
| `SUSER_ID()`           | Gibt die Benutzer-ID des SQL Server-Benutzers zurück.                        |
| `ORIGINAL_LOGIN()`     | Gibt den Benutzernamen des ursprünglichen Anmelders bei der Verbindung mit der Instanz von SQL Server zurück. |

Diese Funktionen sind nützlich, um sicherheitsbezogene Informationen abzufragen und um Zugriffssteuerungen basierend auf Benutzeridentitäten und -rollen in SQL-Abfragen und -Prozeduren zu implementieren. Die Verfügbarkeit und genaue Syntax dieser Funktionen können je nach verwendetem SQL-Dialekt und Datenbankmanagementsystem variieren.

# Mathenmatische Funktionen

Mathematische Funktionen in SQL werden verwendet, um verschiedene mathematische Berechnungen durchzuführen. Diese Funktionen sind hilfreich, um Berechnungen wie Rundungen, trigonometrische Berechnungen, statistische Analysen und vieles mehr durchzuführen. Hier ist eine tabellarische Übersicht einiger gängiger mathematischer Funktionen in SQL:

Entschuldigen Sie das Versehen. Hier ist eine ergänzte Übersicht, die auch die Funktionen `ROOT`, `FLOOR`, `CEILING` und `SQUARE` einschließt:

| Funktion            | Beschreibung                                                                 |
|---------------------|------------------------------------------------------------------------------|
| `ABS()`             | Gibt den absoluten Wert einer Zahl zurück.                                   |
| `CEILING()`         | Rundet eine Zahl auf den nächsten größeren oder gleichen ganzzahligen Wert auf. |
| `FLOOR()`           | Rundet eine Zahl auf den nächsten kleineren oder gleichen ganzzahligen Wert ab. |
| `ROUND()`           | Rundet eine Zahl auf eine bestimmte Anzahl von Dezimalstellen.               |
| `SQRT()`            | Berechnet die Quadratwurzel einer Zahl.                                      |
| `POWER()`           | Hebt eine Zahl auf die angegebene Potenz.                                    |
| `EXP()`             | Berechnet den Wert von e (Basis des natürlichen Logarithmus) hoch eine gegebene Zahl. |
| `LOG()`             | Berechnet den natürlichen Logarithmus einer Zahl.                            |
| `LOG10()`           | Berechnet den Logarithmus zur Basis 10 einer Zahl.                           |
| `PI()`              | Gibt den Wert von π (Pi) zurück.                                             |
| `RAND()`            | Gibt eine zufällige Zahl zwischen 0 und 1 zurück.                            |
| `SIN()`, `COS()`, `TAN()` | Berechnen die Sinus-, Kosinus- bzw. Tangenswerte (trigonometrische Funktionen). |
| `ASIN()`, `ACOS()`, `ATAN()` | Berechnen die Arkussinus-, Arkuskosinus- bzw. Arkustangenswerte.         |
| `SIGN()`            | Gibt das Vorzeichen einer Zahl zurück (+1, 0, -1).                           |
| `MOD()`             | Berechnet den Rest einer Division (Modulo-Operation).                        |
| `SQUARE()`          | Gibt das Quadrat einer Zahl zurück.                                          |
| `CUBE()` oder `POWER(x, 3)` | Gibt die dritte Potenz (Kubik) einer Zahl zurück.                        |

Bitte beachten Sie, dass die Funktion `ROOT()` in vielen SQL-Systemen nicht als eigenständige Funktion existiert. Stattdessen wird üblicherweise `POWER()` oder `SQRT()` für Quadrat- und Kubikwurzeln verwendet. Zum Beispiel kann `POWER(x, 1.0/3)` für die Kubikwurzel verwendet werden. Die genaue Syntax und Verfügbarkeit können je nach SQL-Dialekt und Datenbanksystem variieren.

