# IIF

Die IIF-Funktion in SQL ist eine Art konditionaler Funktion, die ähnlich wie die `IF...ELSE`-Struktur in vielen Programmiersprachen funktioniert. Sie nimmt einen booleschen Ausdruck als Bedingung, wertet diesen aus und gibt je nach Ergebnis einen von zwei Werten zurück. Wenn die Bedingung `TRUE` ergibt, wird der erste Wert zurückgegeben, andernfalls wird der zweite Wert zurückgegeben.

**Syntax der IIF-Funktion:**
```sql
IIF(Boolescher Ausdruck, WertWennTrue, WertWennFalse)
```

**Beispiel für IIF-Funktion:**
Stellen wir uns vor, wir haben eine Tabelle `tbl_Kunden` mit einer Spalte `Adresstyp`. Die IIF-Funktion kann verwendet werden, um zu prüfen, ob der `Adresstyp` eines Kunden "Main Office" ist. Wenn ja, gibt sie "Billing" zurück, ansonsten "Mailing".

```sql
SELECT 
      IIF([Ländername]='Deutschland','Germany',[Ländername]) as Country
FROM [schulungsdatenbank].[dbo].[tbl_Raum]
```

In dieser Abfrage:

- Die `IIF`-Funktion wird in der `SELECT`-Klausel verwendet.
- Sie prüft für jede Zeile in der Tabelle `tbl_Raum`, ob der `Ländername` gleich "Deutschland" ist.
- Wenn der `Ländername` "Deutschland" ist, gibt die Funktion "Germany" zurück, andernfalls den Wert aus `Ländername`.
- Das Ergebnis dieser Auswertung wird in einer neuen Spalte namens `Country` angezeigt.

Die IIF-Funktion ist besonders nützlich für einfache bedingte Logik direkt in SQL-Abfragen, was die Lesbarkeit und Wartung der Abfragen verbessern kann.


# CASE

Die `CASE`-Funktion in SQL ist eine Art bedingte Anweisung (ähnlich der IF-ELSE-Logik in anderen Programmiersprachen), die es ermöglicht, verschiedene Ergebnisse basierend auf spezifischen Bedingungen zurückzugeben. `CASE` kann in verschiedenen Kontexten wie in `SELECT`, `UPDATE`, `DELETE`, `SET` sowie in verschiedenen Klauseln wie `IN`, `WHERE`, `ORDER BY` und `HAVING` verwendet werden. 

Es gibt zwei Arten von `CASE`-Ausdrücken:

1. **Einfacher CASE-Ausdruck**: Vergleicht einen Ausdruck mit einer Reihe von Werten und gibt ein Ergebnis zurück, wenn eine Übereinstimmung gefunden wird.

   Syntax:
   ```sql
   CASE Ausdruck
       WHEN Wert1 THEN Ergebnis1
       WHEN Wert2 THEN Ergebnis2
       ...
       ELSE Standardergebnis
   END
   ```

   Beispiel:
   ```sql
   SELECT 
       Name,
       CASE AbteilungsID
           WHEN 1 THEN 'IT'
           WHEN 2 THEN 'Personal'
           ELSE 'Unbekannt'
       END AS AbteilungsName
   FROM Mitarbeiter;
   ```

2. **Gesuchter CASE-Ausdruck**: Wird verwendet, wenn komplexere Bedingungen erforderlich sind. Er wertet eine Reihe von booleschen Ausdrücken aus, um das Ergebnis zu bestimmen.

   Syntax:
   ```sql
   CASE
       WHEN Bedingung1 THEN Ergebnis1
       WHEN Bedingung2 THEN Ergebnis2
       ...
       ELSE Standardergebnis
   END
   ```

   Beispiel:
   ```sql
      SELECT 
            Zeit_ID,
            DATEPART(quarter,Zeit_ID) as Quartal,
      CASE DATEPART(quarter,Zeit_ID)
            WHEN 1 THEN '1. Quartal'
            WHEN 2 THEN '2. Quartal'
            WHEN 3 THEN '3. Quartal'
            WHEN 4 THEN '4. Quartal'
            ELSE 'Unbekannt'
      END AS AbteilungsName,
            MONTH(Zeit_ID) as Monatsnummer,
            CASE
                  WHEN MONTH(Zeit_ID) IN (1, 2) THEN 'Winter'
                  WHEN MONTH(Zeit_ID) IN (3, 4, 5) THEN 'Frühling'
                  WHEN MONTH(Zeit_ID) IN (6, 7, 8) THEN 'Sommer'
                  WHEN MONTH(Zeit_ID) IN (9, 10, 11) THEN 'Herbst'
                  WHEN MONTH(Zeit_ID) = 12 THEN 'Winter'
            END As Quartal
      FROM
      tbl_Gehälter
   ```

In beiden Fällen kann die `CASE`-Anweisung eine `ELSE`-Klausel enthalten, die ein Standardergebnis liefert, falls keine der Bedingungen erfüllt ist. Wenn `ELSE` nicht angegeben wird und keine Bedingung zutrifft, gibt `CASE` `NULL` zurück. 

`CASE`-Ausdrücke sind besonders nützlich, um komplexe logische Abfragen durchzuführen, Daten basierend auf Bedingungen zu kategorisieren oder um spezifische Ausgaben auf der Grundlage unterschiedlicher Bedingungen zu formatieren.

# CHOOSE

Die CHOOSE-Funktion in SQL ist eine nützliche Funktion, die auf einem Index basiert und einen Wert aus einer Liste von Werten zurückgibt. Sie ähnelt dem Zugriff auf ein Element in einem Array, wobei der Index 1-basiert ist, was bedeutet, dass der Index des ersten Elements 1 ist.

Syntax der CHOOSE-Funktion:

```sql
CHOOSE(Index, Wert1, Wert2, Wert3, ...)
```

Funktionsweise:

> `Index`: Ein **ganzzahliger** Ausdruck, der die Position des zurückzugebenden Werts in der Liste bestimmt.
> `Wert1`, `Wert2`, `Wert3`, ...: Eine Liste von Werten, aus denen ausgewählt wird. Der Wert an der Position, die dem Index entspricht, wird zurückgegeben.

```sql
SELECT 
	Zeit_ID,
	DATEPART(quarter,Zeit_ID) as Quartal,
	CHOOSE ( DATEPART(quarter,Zeit_ID) , 
			'1. Quartal',   /* Wenn Index 1 */
			'2. Quartal',   /* Wenn Index 2 */ 
			'3. Quartal',   /* Wenn Index 3 */
			'4. Quartal'	/* Wenn Index 4 */
			) as Quartal,
	MONTH(Zeit_ID) as Monatsnummer,
	CHOOSE(MONTH(Zeit_ID),
			'Winter','Winter',						/* Wenn `Index`1,2 */
			'Frühling','Frühling','Frühling',		/* Wenn `Index`3,4,5 */
			'Sommer','Sommer','Sommer', 			/* Wenn `Index`6,7,8 */
			'Herbst','Herbst','Herbst','Herbst',	/* Wenn `Index`9,10,11 */
			'Winter'								/* Wenn `Index`12  */
			) as Jahreszeit
FROM
      tbl_Gehälter
```

