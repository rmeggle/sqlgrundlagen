--
-- Aggregatsfunktionen
--
SELECT 
	Mitarbeiter,
    SUM([Umsatz]) AS GesamtUmsatz,
    AVG([Umsatz]) AS DurchschnittsUmsatz,
    MIN([Umsatz]) AS MinimalerUmsatz,
    MAX([Umsatz]) AS MaximalerUmsatz,
    COUNT(*) AS AnzahlVerkäufe
FROM 
    [dbo].[tbl_Verkäufe]
INNER JOIN [dbo].[tbl_Mitarbeiter] on [dbo].[tbl_Mitarbeiter].Mitarbeiter_ID=[dbo].[tbl_Verkäufe].Mitarbeiter_ID
GROUP BY 
    Mitarbeiter
ORDER BY
	Mitarbeiter
GO

