--
-- CROSS JOIN (wer's braucht)
--

SELECT 
    m.Mitarbeiter_ID, 
    m.Mitarbeiter, 
    g.Monatsgehalt
FROM 
    [schulungsdatenbank].[dbo].[tbl_Mitarbeiter] m
CROSS JOIN 
    [schulungsdatenbank].[dbo].[tbl_Geh�lter] g;