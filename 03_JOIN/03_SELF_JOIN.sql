--
-- SELF JOIN
--

SELECT 
    m1.Mitarbeiter AS MitarbeiterName, 
    m2.Mitarbeiter AS VorgesetzterName
FROM 
    [tbl_Mitarbeiter] m1
LEFT JOIN 
    [tbl_Mitarbeiter] m2 ON m1.Parent = m2.Mitarbeiter_ID;
