--
-- INNER JOIN
--

-- Tabelle 1: Mitarbeiter:
SELECT [Mitarbeiter_ID]
      ,[Mitarbeiter]
      ,[Parent]
  FROM [dbo].[tbl_Mitarbeiter]

-- Tabelle 2: Geh�lter
SELECT [Zeit_ID]
      ,[MitarbeiterID]
      ,[Monatsgehalt]
  FROM [dbo].[tbl_Geh�lter]

--
-- Mit einen INNER JOIN an den referenzierbaren Spalten Mitarbeiter_ID verkn�pft:
--
SELECT 
    m.Mitarbeiter_ID,
    m.Mitarbeiter,
    g.Monatsgehalt
FROM 
    [schulungsdatenbank].[dbo].[tbl_Mitarbeiter] m
INNER JOIN 
    [schulungsdatenbank].[dbo].[tbl_Geh�lter] g ON m.Mitarbeiter_ID = g.MitarbeiterID;