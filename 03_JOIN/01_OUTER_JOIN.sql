--
-- LEFT JOIN
--
SELECT 
    m.Mitarbeiter_ID, 
    m.Mitarbeiter, 
    g.Monatsgehalt
FROM 
    [schulungsdatenbank].[dbo].[tbl_Mitarbeiter] m
LEFT JOIN 
    [schulungsdatenbank].[dbo].[tbl_Geh�lter] g ON m.Mitarbeiter_ID = g.MitarbeiterID;

--
-- RIGHT JOIN
--
SELECT 
    m.Mitarbeiter_ID, 
    m.Mitarbeiter, 
    g.Monatsgehalt
FROM 
    [schulungsdatenbank].[dbo].[tbl_Mitarbeiter] m
RIGHT JOIN 
    [schulungsdatenbank].[dbo].[tbl_Geh�lter] g ON m.Mitarbeiter_ID = g.MitarbeiterID;

--
-- Alternative: (bitte nur in Ausnahmen verwenden!)
--
SELECT 
    m.Mitarbeiter_ID, 
    m.Mitarbeiter, 
    g.Monatsgehalt
FROM 
    [schulungsdatenbank].[dbo].[tbl_Mitarbeiter] m , [schulungsdatenbank].[dbo].[tbl_Geh�lter] g
WHERE m.Mitarbeiter_ID = g.MitarbeiterID;



