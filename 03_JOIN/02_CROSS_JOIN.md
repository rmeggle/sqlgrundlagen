# CROSS JOIN

Kreuzprodukte oder kartesische Produkte von zwei Tabellen sind in SQL eine Möglichkeit, alle möglichen Kombinationen von Zeilen aus diesen beiden Tabellen zu erstellen. Sie werden mit einem `CROSS JOIN` realisiert.

**Verständnis von Kreuzprodukten:**
- Ein Kreuzprodukt verbindet jede Zeile der ersten Tabelle mit jeder Zeile der zweiten Tabelle.
- Wenn beispielsweise Tabelle A 3 Zeilen hat und Tabelle B 4 Zeilen, dann resultiert ein Kreuzprodukt in 3 x 4 = 12 Zeilen.

**Verwendung von Kreuzprodukten:**
- Kreuzprodukte werden in der Regel selten verwendet, da sie oft zu großen und unpraktischen Resultsets führen.
- Sie können jedoch nützlich sein, wenn Sie alle möglichen Kombinationen zwischen zwei Datensätzen benötigen.

**CROSS JOIN-Syntax:**
Um ein kartesisches Produkt explizit zu erstellen, verwenden Sie den `CROSS JOIN`-Operator. Hier einige Richtlinien für das Schreiben von Abfragen mit `CROSS JOIN`:

- **Kein Zeilenabgleich**: Da kein Abgleich zwischen den Zeilen der Tabellen erfolgt, wird keine `ON`-Klausel verwendet. (Es wäre ein Fehler, eine `ON`-Klausel mit einem `CROSS JOIN`-Operator zu verwenden.)
- **Tabellennamen-Aufteilung**: Verwenden Sie den `CROSS JOIN`-Operator, um die Tabellennamen gemäß der ANSI-SQL-92-Syntax zu trennen.

**Beispiel:**

Angenommen, Sie haben zwei Tabellen `tbl_A` und `tbl_B` und möchten ein Kreuzprodukt dieser Tabellen erstellen:

```sql
SELECT 
    m.Mitarbeiter_ID, 
    m.Mitarbeiter, 
    g.Monatsgehalt
FROM 
    [schulungsdatenbank].[dbo].[tbl_Mitarbeiter] m
CROSS JOIN 
    [schulungsdatenbank].[dbo].[tbl_Gehälter] g;
```

In diesem Beispiel erstellt die Abfrage ein Resultset, in dem jede Zeile aus `tbl_Mitarbeiter` mit jeder Zeile aus `tbl_Gehälter` kombiniert wird.