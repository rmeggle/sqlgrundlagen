# SELF JOIN

Ein Self Join ist eine SQL-Technik, bei der eine Tabelle mit sich selbst verbunden wird. Dies ist besonders nützlich, wenn Sie Daten aus derselben Tabelle abrufen möchten, die aber in einer Beziehung zueinander stehen, wie beispielsweise in einer Hierarchie oder Referenzierung innerhalb derselben Tabelle.

Um einen Self Join anhand der Tabelle `[tbl_Mitarbeiter]` zu erklären, nehmen wir an, dass diese Tabelle Spalten wie `Mitarbeiter_ID`, `Mitarbeiter` und `Parent` (Vorgesetzter) enthält. Ein typisches Szenario für einen Self Join wäre, die Namen der Mitarbeiter zusammen mit den Namen ihrer direkten Vorgesetzten abzurufen.

Hier ist, wie der Self Join in diesem Fall aufgebaut werden könnte:

1. **Wählen Sie die Tabelle zweimal aus**: Sie verwenden dieselbe Tabelle zweimal in Ihrer Abfrage, behandeln sie jedoch als wären es zwei unterschiedliche Tabellen, indem Sie Aliasnamen verwenden.

2. **Verknüpfen Sie die Tabelle mit sich selbst**: Sie verwenden die `JOIN`-Klausel, um die Tabelle mit sich selbst zu verknüpfen, basierend auf der logischen Beziehung zwischen den Zeilen. Im Fall der `tbl_Mitarbeiter` könnten Sie beispielsweise die `Mitarbeiter_ID` mit der `Parent`-ID verknüpfen.

**Beispielabfrage:**

```sql
SELECT 
    m1.Mitarbeiter AS MitarbeiterName, 
    m2.Mitarbeiter AS VorgesetzterName
FROM 
    [tbl_Mitarbeiter] m1
LEFT JOIN 
    [tbl_Mitarbeiter] m2 ON m1.Parent = m2.Mitarbeiter_ID;
```

In diesem Beispiel:

- `m1` und `m2` sind Aliasnamen für die `[tbl_Mitarbeiter]`, die es uns ermöglichen, die Tabelle zweimal in derselben Abfrage zu verwenden.
- Wir führen einen `LEFT JOIN` durch, um sicherzustellen, dass alle Mitarbeiter aufgelistet werden, auch solche ohne Vorgesetzten (`Parent`).
- `m1.Mitarbeiter` ist der Name des Mitarbeiters und `m2.Mitarbeiter` ist der Name des Vorgesetzten.
- Die Bedingung `m1.Parent = m2.Mitarbeiter_ID` stellt die Verbindung zwischen einem Mitarbeiter und seinem Vorgesetzten her.

Der Self Join ermöglicht es uns, komplexe Beziehungen innerhalb derselben Tabelle abzufragen und darzustellen.