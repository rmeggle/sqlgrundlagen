Äußere Verknüpfungen (Outer Joins) bieten eine erweiterte Perspektive auf Geschäftsdaten, wenn Sie Informationen aus mehreren Tabellen abrufen. Sie unterscheiden sich von Inneren Verknüpfungen (Inner Joins), da sie nicht nur Zeilen mit übereinstimmenden Attributen aus beiden Tabellen abrufen, sondern auch Zeilen, die nur in einer der beiden Tabellen vorhanden sind.

**Verständnis Äußerer Verknüpfungen:**
- Bei einer `INNER JOIN`-Abfrage werden nur die Zeilen zurückgegeben, bei denen Übereinstimmungen in beiden verknüpften Tabellen gefunden werden.
- Mit einer `OUTER JOIN`-Abfrage hingegen erhalten Sie alle übereinstimmenden Zeilen sowie alle Zeilen aus einer oder beiden Tabellen, für die es keine entsprechende Übereinstimmung in der anderen Tabelle gibt.

**Richtlinien für das Schreiben von Abfragen mit OUTER JOIN:**
- **Tabellenaliase**: Sie sollten Tabellenaliase sowohl in der `SELECT`-Liste als auch in der `ON`-Klausel verwenden.
- **Übereinstimmende Spalten**: Ähnlich wie bei `INNER JOIN` kann `OUTER JOIN` für eine oder mehrere übereinstimmende Spalten zwischen den Tabellen verwendet werden.
- **Reihenfolge der Tabellen**: Die Reihenfolge der Tabellen in einer `OUTER JOIN`-Abfrage ist wichtig, da sie bestimmt, ob Sie `LEFT JOIN` oder `RIGHT JOIN` verwenden.
- **Mehrere Tabellenverknüpfungen**: Wenn Sie einen `OUTER JOIN` mit mehreren Tabellen verwenden, achten Sie auf die Behandlung von NULL-Werten. NULL-Werte in den Ergebnissen können die Ergebnisse weiterer Verknüpfungen beeinflussen.
- **Test auf NULL-Werte**: Fügen Sie in der `WHERE`-Klausel nach einem `OUTER JOIN` einen Test auf NULL-Werte hinzu, um nur Zeilen ohne Übereinstimmungen anzuzeigen.
- **FULL OUTER JOIN**: Diese Abfrage gibt alle übereinstimmenden Zeilen zwischen zwei Tabellen sowie alle Zeilen ohne Übereinstimmung in beiden Tabellen zurück. Sie wird seltener verwendet.
- **Reihenfolge der Ergebnisse**: Ohne eine `ORDER BY`-Klausel ist die Reihenfolge, in der die Zeilen zurückgegeben werden, nicht vorhersehbar.

**Beispiel anhand Ihrer Tabellen:**

Angenommen, Sie möchten alle Mitarbeiter aus `tbl_Mitarbeiter` und ihre Gehälter aus `tbl_Gehälter` auflisten, einschließlich derer ohne Gehaltsdaten:

```sql
SELECT 
    m.Mitarbeiter_ID, 
    m.Mitarbeiter, 
    g.Monatsgehalt
FROM 
    [schulungsdatenbank].[dbo].[tbl_Mitarbeiter] m
LEFT JOIN 
    [schulungsdatenbank].[dbo].[tbl_Gehälter] g ON m.Mitarbeiter_ID = g.MitarbeiterID;
```

In dieser `LEFT JOIN`-Abfrage erhalten Sie alle Mitarbeiter, unabhängig davon, ob sie in `tbl_Gehälter` aufgeführt sind. Mitarbeiter ohne Gehaltsdaten werden mit NULL-Werten im Feld `Monatsgehalt` aufgelistet.

# Was ist der Unerschied zwischen z.B. LEFT und LEFT OUTER JOIN?

Gar keiner: Der Begriff `OUTER` wird oft hinzugefügt, um die Unterscheidung zu den INNER JOINs zu verdeutlichen, aber in der Praxis ist die Verwendung von `OUTER` optional und ändert nichts an der Funktionsweise des JOINs.


