Innerhalb von T-SQL-Abfragen ist der `INNER JOIN` eine häufig genutzte Methode, um Daten aus mehreren Tabellen zusammenzuführen. Diese Art von Verknüpfung ist besonders nützlich in Datenbanken, die nach dem Prinzip der Normalisierung aufgebaut sind. Durch einen `INNER JOIN` können Sie Daten aus verschiedenen Tabellen abrufen, die in Beziehung zueinander stehen. Der Prozess beginnt mit einem kartesischen Produkt der beteiligten Tabellen, gefolgt von einem Filtern der Ergebnisse gemäß einem bestimmten Kriterium oder Prädikat.

Lassen Sie uns dies anhand der von Ihnen genannten Tabellen verdeutlichen:

Sie haben zwei Tabellen: `tbl_Mitarbeiter` und `tbl_Gehälter`. Nehmen wir an, Sie möchten Informationen zu jedem Mitarbeiter zusammen mit dessen Monatsgehalt abrufen. 

Die Tabelle `tbl_Mitarbeiter` enthält Spalten wie `Mitarbeiter_ID`, `Mitarbeiter` und `Parent`. Die Tabelle `tbl_Gehälter` enthält `Zeit_ID`, `MitarbeiterID` und `Monatsgehalt`. 

Um diese beiden Tabellen mithilfe eines `INNER JOIN` zu verknüpfen, können Sie die `Mitarbeiter_ID` aus der Tabelle `tbl_Mitarbeiter` mit der `MitarbeiterID` aus der Tabelle `tbl_Gehälter` abgleichen. So sieht die Abfrage aus:

```sql
SELECT 
    m.Mitarbeiter_ID,
    m.Mitarbeiter,
    g.Monatsgehalt
FROM 
    [schulungsdatenbank].[dbo].[tbl_Mitarbeiter] m
INNER JOIN 
    [schulungsdatenbank].[dbo].[tbl_Gehälter] g ON m.Mitarbeiter_ID = g.MitarbeiterID;
```

In dieser Abfrage:
- `INNER JOIN` verknüpft die beiden Tabellen basierend auf der Bedingung, dass die `Mitarbeiter_ID` in der Tabelle `tbl_Mitarbeiter` der `MitarbeiterID` in der Tabelle `tbl_Gehälter` entspricht.
- Nur die Zeilen, bei denen eine Übereinstimmung gefunden wird, werden zurückgegeben. Das bedeutet, dass nur Mitarbeiterdaten mit entsprechenden Gehaltsinformationen angezeigt werden.