# Unterabfragen (subquery)

Unterabfragen, auch Subqueries genannt, sind Abfragen, die innerhalb einer anderen SQL-Abfrage eingebettet sind. Sie werden häufig verwendet, um komplexere Abfragen zu erstellen, indem sie es ermöglichen, Daten zu filtern, zu aggregieren oder zu manipulieren, bevor diese Daten in der Hauptabfrage verwendet werden.

**Eigenschaften von Unterabfragen:**

1. **Einbettung**: Eine Unterabfrage ist in der Regel in Klammern gesetzt und findet sich innerhalb der `SELECT`, `FROM`, `WHERE` oder `HAVING`-Klausel einer übergeordneten Abfrage.

2. **Unabhängigkeit**: Sie wird zuerst ausgeführt, und das Ergebnis wird von der umgebenden (äußeren) Abfrage verwendet.

3. **Rückgabewerte**: Unterabfragen können einen einzelnen Wert, eine Reihe von Werten oder eine ganze Ergebnismenge zurückgeben, abhängig vom Kontext ihrer Verwendung.

4. **Kontextabhängigkeit**: Die Unterabfrage kann sich auf Spalten der äußeren Abfrage beziehen, was oft bei korrelierten Unterabfragen der Fall ist.

**Typische Verwendungen von Unterabfragen:**

1. **In der `WHERE`-Klausel**: 

Angenommen, Sie möchten alle Mitarbeiter aus tbl_Mitarbeiter abrufen, die ein Gehalt über dem Durchschnittsgehalt aller Mitarbeiter haben. Die Unterabfrage berechnet das Durchschnittsgehalt, während die Hauptabfrage die Mitarbeiterdaten abruft.  

```sql
SELECT 
m.Mitarbeiter_ID,
m.Mitarbeiter
FROM 
[tbl_Mitarbeiter] m
WHERE 
(SELECT AVG(Monatsgehalt) FROM [tbl_Gehälter]) < (SELECT Monatsgehalt FROM [tbl_Gehälter] WHERE MitarbeiterID = m.Mitarbeiter_ID);

```

2. **In der `FROM`-Klausel**: 

Hier erstellen wir eine abgeleitete Tabelle mit den durchschnittlichen Gehältern der Mitarbeiter und verwenden diese in der Hauptabfrage, um die Mitarbeiterdaten abzurufen.
   
Beispiel:
```sql
SELECT 
m.Mitarbeiter_ID,
m.Mitarbeiter,
g.Durchschnittsgehalt
FROM 
[tbl_Mitarbeiter] m
JOIN 
(SELECT MitarbeiterID, AVG(Monatsgehalt) AS Durchschnittsgehalt FROM [tbl_Gehälter] GROUP BY MitarbeiterID) g ON m.Mitarbeiter_ID = g.MitarbeiterID;

```

3. **In der `SELECT`-Klausel**: 

Hier verwenden wir eine korrelierte Unterabfrage, um jeden Mitarbeiter mit dem Gesamtgehalt, das er über alle verfügbaren Monate erhalten hat, aufzulisten.
   
Beispiel:
```sql
SELECT 
m.Mitarbeiter_ID,
m.Mitarbeiter,
(SELECT SUM(Monatsgehalt) FROM [tbl_Gehälter] g WHERE g.MitarbeiterID = m.Mitarbeiter_ID) AS Gesamtgehalt
FROM 
[tbl_Mitarbeiter] m;

```

1. **Korrelierte Unterabfragen**: Hier bezieht sich die Unterabfrage auf eine Spalte der äußeren Abfrage, wodurch für jede Zeile der äußeren Abfrage eine separate Auswertung erfolgt.

Beispiel:
```sql
SELECT 
m.Mitarbeiter_ID,
m.Mitarbeiter
FROM 
[tbl_Mitarbeiter] m
WHERE 
EXISTS (SELECT 1 FROM [tbl_Gehälter] g WHERE g.MitarbeiterID = m.Mitarbeiter_ID);

```

Unterabfragen können die Flexibilität und Leistungsfähigkeit von SQL-Abfragen erheblich erweitern, insbesondere wenn es um komplexe Datenmanipulationen und Analysen geht.

Bitte prüfen Sie daher unbedingt, ob Sie nicht das identische Ergebnis mit einem geschickten JOIN herstellen können.