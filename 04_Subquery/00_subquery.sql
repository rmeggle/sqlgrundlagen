--
-- Subquery im FROM
--
SELECT 
m.Mitarbeiter_ID,
m.Mitarbeiter,
g.Durchschnittsgehalt
FROM 
[tbl_Mitarbeiter] m
JOIN 
(SELECT MitarbeiterID, AVG(Monatsgehalt) AS Durchschnittsgehalt FROM [tbl_Geh�lter] GROUP BY MitarbeiterID) g ON m.Mitarbeiter_ID = g.MitarbeiterID;

--
-- Subquery im SELECT
--

SELECT 
m.Mitarbeiter_ID,
m.Mitarbeiter,
(SELECT SUM(Monatsgehalt) FROM [tbl_Geh�lter] g WHERE g.MitarbeiterID = m.Mitarbeiter_ID) AS Gesamtgehalt
FROM 
[tbl_Mitarbeiter] m;

--
-- Korrelierte subquery mit EXISTS
--
SELECT 
m.Mitarbeiter_ID,
m.Mitarbeiter
FROM 
[tbl_Mitarbeiter] m
WHERE 
EXISTS (SELECT 1 FROM [tbl_Geh�lter] g WHERE g.MitarbeiterID = m.Mitarbeiter_ID);
