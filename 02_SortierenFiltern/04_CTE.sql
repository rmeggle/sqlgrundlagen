--
-- CTE
--

WITH CTESorted AS (
    SELECT [Lšndername], [Ortsname]
    FROM tbl_Raum
)
SELECT DISTINCT [Lšndername]
FROM CTESorted
ORDER BY [Lšndername]


--
-- Rekursive CTE eine Hierarchie von Mitarbeitern und ihren Vorgesetzten
--
WITH MitarbeiterCTE AS (
    -- Ankermitglied: Auswahl aller Mitarbeiter, die ihre eigenen Vorgesetzten sind
    SELECT Mitarbeiter_ID, Mitarbeiter, Parent, CAST(Mitarbeiter AS VARCHAR(MAX)) AS Hierarchie
    FROM tbl_Mitarbeiter
    WHERE Mitarbeiter_ID = Parent

    UNION ALL

    -- Rekursives Mitglied: Verbindet Mitarbeiter mit ihren Vorgesetzten, vermeidet aber Selbstreferenzierung
    SELECT m.Mitarbeiter_ID, m.Mitarbeiter, m.Parent, CAST(c.Hierarchie + ' -> ' + m.Mitarbeiter AS VARCHAR(MAX))
    FROM tbl_Mitarbeiter m
    INNER JOIN MitarbeiterCTE c ON m.Parent = c.Mitarbeiter_ID
    WHERE m.Mitarbeiter_ID <> m.Parent
)
SELECT *
FROM MitarbeiterCTE;