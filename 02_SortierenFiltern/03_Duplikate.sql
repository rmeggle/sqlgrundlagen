--
-- Entfernen von Duplikaten
--

--
-- Mit Duplikate
--
SELECT [L�ndername]
FROM tbl_Raum
ORDER BY [L�ndername]

--
-- Ohne Duplikate
--
SELECT DISTINCT [L�ndername]
FROM tbl_Raum
ORDER BY [L�ndername]

--
-- Diese Abfrage f�hrt zu einem Fehler: ORDER BY-Elemente m�ssen in der Auswahlliste angezeigt werden, wenn SELECT DISTINCT angegeben wird.
--
/*
SELECT DISTINCT [L�ndername]
FROM tbl_Raum
ORDER BY [Ortsname]
*/