--
-- Sortieren der Ergebnismenge
--

SELECT 
	Artikel,
	WG2_Bezeichnung,
	WG1_Bezeichnung
FROM 
[dbo].[tbl_Produkt]
ORDER BY 
	Artikel DESC,
	WG2_Bezeichnung ASC,
	WG1_Bezeichnung ASC