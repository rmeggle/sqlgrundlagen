# Seitenergebnisse mit OFFSET

Die `OFFSET-FETCH`-Klausel ist eine nützliche Erweiterung der `ORDER BY`-Klausel in SQL Server, die es Ihnen ermöglicht, einen bestimmten Bereich von Zeilen aus den Ergebnissen Ihrer Abfrage zurückzugeben. Diese Funktion ist besonders hilfreich für die Implementierung einer seitenweisen Anzeige von Daten, wie sie häufig in Web- und Anwendungsseiten verwendet wird.

Syntaxbeschreibung:
```sql
OFFSET { integer_constant | offset_row_count_expression } { ROW | ROWS }
[FETCH { FIRST | NEXT } {integer_constant | fetch_row_count_expression } { ROW | ROWS } ONLY]
```

**Verwendung von OFFSET-FETCH:**

- **Offset**: Dies ist der Startpunkt in der Ergebnismenge, von dem aus Zeilen zurückgegeben werden. Mit anderen Worten, Sie geben an, wie viele Zeilen übersprungen werden sollen, bevor die tatsächliche Ausgabe der Ergebnisse beginnt.
- **Fetch**: Dieser Wert gibt an, wie viele Zeilen nach dem Offset zurückgegeben werden sollen.

Es ist wichtig zu beachten, dass jede Ausführung einer Abfrage mit einer `OFFSET-FETCH`-Klausel unabhängig ist. Es gibt keinen serverseitigen Zustand, der zwischen den einzelnen Abfragen aufrechterhalten wird. Das bedeutet, dass Sie bei der Implementierung einer seitenweisen Navigation clientseitig den Überblick über Ihre aktuelle Position im Resultset behalten müssen.

**Syntax der OFFSET-FETCH-Klausel:**

Die Syntax für die `OFFSET-FETCH`-Klausel, die technisch gesehen ein Teil der `ORDER BY`-Klausel ist, sieht folgendermaßen aus:

```sql
SELECT [Firma],[Ortsname]
FROM tbl_Raum
ORDER BY [Ländername]
OFFSET 0 ROWS
FETCH NEXT 10 ROWS ONLY;
```

- `rows_to_skip` ist die Anzahl der Zeilen, die übersprungen werden sollen.
- `rows_to_fetch` ist die Anzahl der Zeilen, die nach dem Überspringen der ersten `rows_to_skip` Zeilen zurückgegeben werden sollen.

Diese Klausel ermöglicht es Ihnen, in einem großen Datensatz zu navigieren, indem Sie genau festlegen, welcher Teil der Daten zurückgegeben werden soll, was insbesondere für Anwendungen mit großen Datenmengen oder für die Erstellung von Benutzeroberflächen mit Blätterfunktionen nützlich ist.

