# CTE

Ein Gemeinsamer Tabellenausdruck (Common Table Expression, CTE) ist ein mächtiges Feature in SQL, das temporäre Resultsets erzeugt, welche innerhalb einer einzelnen Abfrage, wie `SELECT`, `INSERT`, `UPDATE` oder `DELETE`, verwendet werden können. CTEs bieten eine strukturiertere und oft leserlichere Alternative zu Unterabfragen und können auch rekursive Abfragen unterstützen.

**Grundstruktur eines CTE:**

```sql
WITH CTE_Name AS (
    -- Ihre CTE-Abfrage hier
)
-- Ihre Hauptabfrage, die den CTE verwendet
```

- `CTE_Name` ist der Name des CTE, den Sie in Ihrer Hauptabfrage verwenden.
- Die CTE-Abfrage ist eine `SELECT`-Anweisung, die das Resultset definiert, auf das Sie in der Hauptabfrage zugreifen möchten.

**Vorteile von CTEs:**

1. **Lesbarkeit und Wartbarkeit**: CTEs machen komplexe Abfragen leichter verständlich und wartbar, indem sie komplexe Unterabfragen in separate, logische Abschnitte aufteilen.

2. **Wiederverwendbarkeit innerhalb einer Abfrage**: Ein CTE kann in einer Abfrage mehrfach verwendet werden, was Redundanz verringert und die Klarheit verbessert.

3. **Ermöglicht rekursive Abfragen**: Rekursive CTEs sind nützlich, um hierarchische oder rekursive Daten zu verarbeiten, wie zum Beispiel Baumstrukturen.

4. **Einfachere Fehlerbehebung**: Sie können einen CTE isoliert testen und überprüfen, was die Fehlersuche vereinfacht.

**Beispiel für einen einfachen CTE:**

```sql
WITH CTESorted AS (
    SELECT [Ländername], [Ortsname]
    FROM tbl_Raum
)
SELECT DISTINCT [Ländername]
FROM CTESorted
ORDER BY [Ländername];
```

In diesem Beispiel:

* Zuerst erstellen Sie mit einem CTE eine temporäre Tabelle SortedRaum, die alle erforderlichen Daten enthält.
* Anschließend wählen Sie mit SELECT DISTINCT [Ländername] eindeutige Ländernamen aus dem CTE aus.
* Schließlich ordnen Sie das Ergebnis mit ORDER BY [Ländername].
  

Beachten Sie, dass die Sortierung in diesem Fall auf der Basis der eindeutigen [Ländername] erfolgt, da [Ortsname] nicht in der finalen SELECT-Liste enthalten ist. Wenn Sie jedoch eine Sortierung basierend auf [Ortsname] beibehalten wollen, wird die Situation komplizierter und kann ein anderes Design der Abfrage erfordern, das möglicherweise weitere Funktionen oder fortgeschrittenere SQL-Techniken verwendet.

**Rekursive CTEs:**

Zusammengefasst erstellt eine rekursive CTE eine Hierarchie z.B. von Mitarbeitern und ihren Vorgesetzten, wobei jeder Mitarbeiter der Hierarchie entsprechend seiner Position in der Organisationsstruktur hinzugefügt wird. Die resultierende Hierarchie wird in einer `Hierarchie`-Spalte als Pfad dargestellt, der die Beziehung von Mitarbeitern zu ihren Vorgesetzten zeigt.

```sql
WITH MitarbeiterCTE AS (
    -- Ankermitglied: Auswahl aller Mitarbeiter, die ihre eigenen Vorgesetzten sind
    SELECT Mitarbeiter_ID, Mitarbeiter, Parent, CAST(Mitarbeiter AS VARCHAR(MAX)) AS Hierarchie
    FROM tbl_Mitarbeiter
    WHERE Mitarbeiter_ID = Parent

    UNION ALL

    -- Rekursives Mitglied: Verbindet Mitarbeiter mit ihren Vorgesetzten, vermeidet aber Selbstreferenzierung
    SELECT m.Mitarbeiter_ID, m.Mitarbeiter, m.Parent, CAST(c.Hierarchie + ' -> ' + m.Mitarbeiter AS VARCHAR(MAX))
    FROM tbl_Mitarbeiter m
    INNER JOIN MitarbeiterCTE c ON m.Parent = c.Mitarbeiter_ID
    WHERE m.Mitarbeiter_ID <> m.Parent
)
SELECT *
FROM MitarbeiterCTE;
```

Diese Common Table Expression (CTE) ist ein Beispiel für eine rekursive CTE in SQL, die zur Darstellung von Hierarchien verwendet wird, wie z.B. eine Mitarbeiter-Vorgesetzten-Hierarchie in einem Unternehmen. Hier wird sie verwendet, um eine Hierarchie von Mitarbeitern und ihren Vorgesetzten zu erstellen. Die Abfrage besteht aus zwei Hauptteilen:

1. **Ankermitglied**: Dies ist der Ausgangspunkt der Rekursion. Es wählt die Anfangszeilen aus, auf denen die rekursive Abfrage aufbaut.

   - `SELECT Mitarbeiter_ID, Mitarbeiter, Parent, CAST(Mitarbeiter AS VARCHAR(MAX)) AS Hierarchie`
     - Hier werden die `Mitarbeiter_ID`, der Name des Mitarbeiters (`Mitarbeiter`), die `Parent`-ID (Vorgesetzter) und eine Hierarchiezeichenkette ausgewählt, die zu Beginn nur den Namen des Mitarbeiters enthält.
   - `FROM tbl_Mitarbeiter`
     - Die Daten werden aus der Tabelle `tbl_Mitarbeiter` abgerufen.
   - `WHERE Mitarbeiter_ID = Parent`
     - Diese Bedingung wählt nur die Mitarbeiter aus, die ihre eigenen Vorgesetzten sind (d.h., deren `Mitarbeiter_ID` gleich ihrer `Parent`-ID ist). Dies dient als Anfangspunkt für die Hierarchie.

2. **Rekursives Mitglied**: Dieser Teil der CTE baut auf dem Ankermitglied auf und erweitert die Hierarchie rekursiv.

   - `SELECT m.Mitarbeiter_ID, m.Mitarbeiter, m.Parent, CAST(c.Hierarchie + ' -> ' + m.Mitarbeiter AS VARCHAR(MAX))`
     - Es wählt die gleichen Spalten wie das Ankermitglied, aber es erweitert die `Hierarchie`-Spalte um den Namen des Mitarbeiters in jedem Rekursionsschritt.
   - `FROM tbl_Mitarbeiter m INNER JOIN MitarbeiterCTE c ON m.Parent = c.Mitarbeiter_ID`
     - Dieser JOIN verbindet die Mitarbeiter-Tabelle mit der bisher aufgebauten Hierarchie. Es verknüpft Mitarbeiter mit ihren Vorgesetzten basierend auf der `Parent`-ID.
   - `WHERE m.Mitarbeiter_ID <> m.Parent`
     - Diese Bedingung vermeidet Selbstreferenzierung, indem sie sicherstellt, dass nur Mitarbeiter ausgewählt werden, deren `Mitarbeiter_ID` nicht ihrer `Parent`-ID entspricht.



Rekursive CTEs sind besonders nützlich für die Arbeit mit hierarchischen Daten. Sie bestehen aus zwei Teilen: dem Ankermitglied (eine initiale `SELECT`-Anweisung, die den Basisfall definiert) und dem rekursiven Mitglied (eine `SELECT`-Anweisung, die sich auf den CTE selbst bezieht und die Rekursion definiert).

Zusammenfassend bieten CTEs eine flexible und leistungsstarke Möglichkeit, komplexe Abfragen in SQL strukturiert und übersichtlich zu gestalten.

