--
-- Vergleich von Zahlenwerten
--
SELECT 
	* 
FROM [dbo].[tbl_Verk�ufe]
WHERE Umsatz = 150 AND Menge > 10

--
-- BETWEEN
--
SELECT 
	* 
FROM [dbo].[tbl_Verk�ufe]
WHERE Umsatz BETWEEN 100 AND 150

--
-- Bei Zeichenketten
--
SELECT 
	* 
FROM [dbo].[tbl_Raum]
WHERE Erdteilname='Europa'

--
-- LIKE
--

SELECT 
	* 
FROM [dbo].[tbl_Raum]
WHERE L�ndername LIKE 'D%'

--
-- Weitere Beispiele:
--

-- alle Eintr�ge, bei denen der `L�ndername` mit 'Deutsch' beginnt
SELECT * FROM [dbo].[tbl_Raum] WHERE [L�ndername] LIKE 'Deutsch%'

-- Eintr�ge, bei denen die `Stadt` 'Berlin' ist, wobei das zweite Zeichen beliebig sein kann
SELECT * FROM [dbo].[tbl_Raum] WHERE [Ortsname] LIKE 'B_rlin'

-- alle Eintr�ge, bei denen der `L�ndername` mit 'B' oder 'S' beginnt
SELECT * FROM [dbo].[tbl_Raum] WHERE [L�ndername] LIKE '[BS]%'

-- alle Eintr�ge, bei denen der `L�ndername` *nicht* mit 'D' beginnt
SELECT * FROM [dbo].[tbl_Raum] WHERE [L�ndername] LIKE '[^D]%'


--
-- Der Mengenvergleichsoperator IN
--
SELECT * FROM [dbo].[tbl_Raum] 
WHERE [L�ndername] = 'Mexiko' OR [L�ndername] = 'Schweden'

-- alternativ:
SELECT * FROM [dbo].[tbl_Raum] 
WHERE [L�ndername] IN ('Mexiko', 'Schweden')
