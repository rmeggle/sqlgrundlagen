# Filtern von Datenmengen

Die einfachsten `SELECT`-Befehle in SQL, die nur `SELECT` und `FROM` enthalten, betrachten jede Zeile in einer Tabelle. Um jedoch nur bestimmte Zeilen auszuwählen, verwenden Sie eine `WHERE`-Klausel. Diese legt fest, welche Zeilen in den Ergebnissen erscheinen sollen und kann die Menge der zurückgegebenen Daten verringern.

**Struktur der WHERE-Klausel:**

Die `WHERE`-Klausel setzt sich aus einem oder mehreren Kriterien zusammen. Diese Kriterien, auch "Prädikate" genannt, testen, ob jede Zeile bestimmte Bedingungen erfüllt. Eine Zeile wird nur dann in den Ergebnissen angezeigt, wenn das Kriterium der `WHERE`-Klausel für diese Zeile zutrifft, das heißt, wenn das Kriterium wahr ("TRUE") ist.

Die Bedingungen in einer `WHERE`-Klausel werden oft mit grundlegenden Vergleichsoperatoren formuliert. Zum Beispiel können Sie überprüfen, ob eine Spalte einen bestimmten Wert hat, größer oder kleiner als ein bestimmter Wert ist und so weiter. Nur die Zeilen, die diesen Bedingungen entsprechen, werden in den Ergebnissen der Abfrage zurückgegeben.

Übrsicht über Vergleichsoperatoren: 

| Operator | Beschreibung             | Geeignet für Datentypen         |
|----------|--------------------------|---------------------------------|
| =        | Gleich                    | Alle vergleichbaren Datentypen  |
| <>       | Ungleich                  | Alle vergleichbaren Datentypen  |
| >        | Größer als                | Zahlen, Datum/Zeit              |
| <        | Kleiner als               | Zahlen, Datum/Zeit              |
| >=       | Größer als oder gleich    | Zahlen, Datum/Zeit              |
| <=       | Kleiner als oder gleich   | Zahlen, Datum/Zeit              |
| LIKE     | Musterabgleich            | Zeichenketten                   |
| IN       | In einer Liste von Werten | Alle vergleichbaren Datentypen  |
| BETWEEN  | Zwischen zwei Werten      | Zahlen, Datum/Zeit, Zeichenketten|
| IS NULL  | Ist NULL                  | Alle Datentypen                 |

Dies kann auch zusammen mit den logischen Operatoren kombiniert werden: 

| Operator | Beschreibung                                               | Verwendung                   |
|----------|-----------------------------------------------------------|------------------------------|
| AND      | Gibt TRUE zurück, wenn beide Bedingungen wahr sind         | Verknüpfung von zwei oder mehr Bedingungen, die alle erfüllt sein müssen |
| OR       | Gibt TRUE zurück, wenn mindestens eine Bedingung wahr ist  | Verknüpfung von zwei oder mehr Bedingungen, von denen mindestens eine erfüllt sein muss |
| NOT      | Kehrt das Ergebnis einer Bedingung um                      | Verwendung vor einer Bedingung, um das Gegenteil des Ergebnisses zu erhalten |

Beispiel: 
```sql
SELECT 
	* 
FROM [dbo].[tbl_Verkäufe]
WHERE Umsatz = 150 AND Menge > 10
```

Bei dem Vergleich mit BETWEEN ist nur das AND erlaubt: 
```sql
SELECT 
	* 
FROM [dbo].[tbl_Verkäufe]
WHERE Umsatz BETWEEN 100 AND 150
```

# LIKE

Die `LIKE`-Klausel in SQL wird für den Musterabgleich in Zeichenketten verwendet. Sie ist besonders nützlich, um Daten zu finden, die einem bestimmten Muster entsprechen, aber nicht genau übereinstimmen. `LIKE`-Abfragen werden häufig in der `WHERE`-Klausel verwendet. 

Hier ist eine kurze Übersicht über die Verwendung von `LIKE`:

**1. Einfache Platzhalter:**
- `%`: Der Prozentzeichen-Platzhalter repräsentiert eine beliebige Zeichenfolge beliebiger Länge (einschließlich null Länge).
- `_`: Der Unterstrich-Platzhalter steht für genau ein Zeichen.

**Beispiele:**
- `LIKE 'a%'`: Findet alle Werte, die mit "a" beginnen.
- `LIKE '%a'`: Findet alle Werte, die mit "a" enden.
- `LIKE '%or%'`: Findet alle Werte, die "or" irgendwo enthalten.
- `LIKE '_r%'`: Findet alle Werte, bei denen das zweite Zeichen "r" ist.

Eine Übersicht über mögliche Platzhalter:
| Platzhalter | Beschreibung                                                         |
|-------------|----------------------------------------------------------------------|
| %           | Steht für eine beliebige Zeichenfolge beliebiger Länge (auch null).  |
| _           | Steht für genau ein beliebiges Zeichen.                              |
| [charlist]  | Steht für jedes einzelne Zeichen in charlist.                        |
| [^charlist] oder [!charlist] | Steht für jedes Zeichen, das nicht in charlist ist. |


**2. Verwendung von Escape-Zeichen:**
Falls `%` oder `_` als normale Zeichen und nicht als Platzhalter verwendet werden sollen, muss ein Escape-Zeichen definiert werden.

**Beispiele**
Auf Basis Ihrer Tabelle `tbl_Raum` hier einige Beispiele, wie Sie die `LIKE`-Klausel mit verschiedenen Platzhaltern verwenden können:

1. **Einfacher Platzhalter `%`**:
   - `SELECT * FROM [dbo].[tbl_Raum] WHERE [Ländername] LIKE 'Deutsch%'`: Findet alle Einträge, bei denen der `Ländername` mit 'Deutsch' beginnt (zum Beispiel 'Deutschland').

2. **Einfacher Platzhalter `_`**:
   - `SELECT * FROM [dbo].[tbl_Raum] WHERE [Stadt] LIKE 'B_rlin'`: Findet Einträge, bei denen die `Stadt` 'Berlin' ist, wobei das zweite Zeichen beliebig sein kann (würde 'Berlin' finden, wenn es zum Beispiel 'Barlin' oder 'Berlin' gäbe).

3. **Zeichenbereich `[ ]`**:
   - `SELECT * FROM [dbo].[tbl_Raum] WHERE [Ländername] LIKE '[BS]%`': Findet alle Einträge, bei denen der `Ländername` mit 'B' oder 'S' beginnt (zum Beispiel 'Brasilien', 'Schweden').

4. **Negierter Zeichenbereich `[^ ]` oder `[! ]`**:
   - `SELECT * FROM [dbo].[tbl_Raum] WHERE [Ländername] LIKE '[^D]%'`: Findet alle Einträge, bei denen der `Ländername` **nicht** mit 'D' beginnt.

Bitte beachten Sie, dass die genaue Syntax und Funktionsweise dieser Platzhalter je nach verwendetem SQL-System variieren kann.


# Mengenvergleich IN

In einigen Fällen ist es übesichtlicher, manchmal auch schneller (manchmal auch wesentlich langsamer), wenn man statt `OR` den Mengenvergleichsoperator `IN` verwendet. 

Beispiel:

