# Duplikate

Wenn Sie aus einer Datenbanktabelle eine Auswahl von Spalten abfragen, kann es passieren, dass die resultierenden Zeilen nicht eindeutig sind, auch wenn die vollständigen Zeilen in der Tabelle einzigartig sind. Dies tritt häufig auf, wenn nur ein Subset von Spalten abgefragt wird, das nicht ausreicht, um jede Zeile eindeutig zu identifizieren.

Nehmen wir als Beispiel eine Tabelle mit Lieferantendaten. In dieser Tabelle könnten Stadt und Bundesland (oder Provinz) in Kombination einzigartig sein, sodass jeder Stadt nur ein Lieferant zugeordnet ist. Wenn Sie jedoch eine Abfrage erstellen, die nur die Städte und Länder/Regionen anzeigt, in denen sich Lieferanten befinden, könnte es sein, dass diese Ergebnisse nicht eindeutig sind. Dies liegt daran, dass mehrere Lieferanten in derselben Stadt und demselben Bundesland ansässig sein könnten.

Betrachten Sie beispielsweise die folgende Abfrage:
```sql
SELECT [Ländername]
FROM tbl_Raum
ORDER BY [Ländername]
```

In dieser Abfrage werden alle Ländernamen aus der Tabelle tbl_Raum ausgewählt. Wenn mehrere Ländernamen genannt sind, so erscheinen sie mehrfach. 
Wenn Sie diese Duplikate aussortiert haben wollen, so fügen Sie `DISTINCT` hinzu:

```sql
SELECT DISTINCT [Ländername]
FROM tbl_Raum
ORDER BY [Ländername]
```

Beachten Sie bitte, dass ORDER BY-Elemente in der Auswahlliste angezeigt werden müssen, wenn SELECT DISTINCT angegeben wird. Die folgende Abfrage ergibt daher einen Fehler:

```sql
SELECT DISTINCT [Ländername]
FROM tbl_Raum
ORDER BY [Ortsname]
```

Dies liese sich nur mit einer kompexen Subquery, gespeicherten Prozedur mit CURSOR (also sehr fortgeschrittene Ansätze), oder viel leichter mittels cte lösen. Siehe dazu das nächste Kapitel. Die Lösung wäre wie folgt: 

Lösungsansatz mit CTE:

```sql
WITH CTESorted AS (
    SELECT [Ländername], [Ortsname]
    FROM tbl_Raum
)
SELECT DISTINCT [Ländername]
FROM CTESorted
ORDER BY [Ländername];
```
