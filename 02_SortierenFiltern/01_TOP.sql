--
-- TOP <Anzahl>
--
SELECT TOP 10
	Mitarbeiter_ID,
	Umsatz 
FROM [dbo].[tbl_Verk�ufe]
ORDER BY Umsatz DESC

--
-- WITH TIES
--
SELECT TOP 10 WITH TIES
	Mitarbeiter_ID,
	Umsatz 
FROM [dbo].[tbl_Verk�ufe]
ORDER BY Umsatz DESC

--
-- die besten - in Prozent
--
SELECT TOP 10 PERCENT
	Mitarbeiter_ID,
	Umsatz 
FROM [dbo].[tbl_Verk�ufe]
ORDER BY Umsatz DESC
