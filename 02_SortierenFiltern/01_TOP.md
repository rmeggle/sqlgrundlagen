# TOP ten

Die `TOP`-Klausel stellt eine spezielle Erweiterung von Microsoft für die `SELECT`-Anweisung dar. Mit ihr lässt sich festlegen, wie viele Zeilen aus einer Abfrage zurückgegeben werden sollen. Diese Begrenzung kann entweder durch eine feste Anzahl von Zeilen, angegeben als positive ganze Zahl, oder durch einen Prozentsatz der betroffenen Zeilen definiert werden. Die Anzahl der Zeilen kann dabei als fester Wert oder als berechneter Ausdruck festgelegt werden. Obwohl `TOP` oft in Kombination mit der `ORDER BY`-Klausel verwendet wird, um die obersten Zeilen einer sortierten Ergebnismenge zu erhalten, kann es auch auf unsortierte Daten angewandt werden. 

Beispielsweise können Sie die `TOP`-Klausel verwenden, um nur die ersten 10 Zeilen oder die obersten 10 Prozent der Ergebnisse einer Abfrage zurückzugeben.

```sql
SELECT TOP 10
	Mitarbeiter_ID,
	Umsatz 
FROM [dbo].[tbl_Verkäufe]
```

# TOP WITH TIES

Die Klausel WITH TIES in SQL Server wird in Verbindung mit der TOP-Klausel verwendet. Sie ermöglicht es Ihnen, zusätzliche Zeilen zurückzugeben, die den gleichen Wert in der Spalte haben, nach der sortiert wird, wie die letzte Zeile, die durch die TOP-Klausel zurückgegeben wird.

Ohne WITH TIES gibt die TOP-Klausel eine festgelegte Anzahl von Zeilen zurück, basierend auf den Kriterien, die Sie angeben (zum Beispiel TOP 10). Wenn jedoch Zeilen existieren, die denselben Wert in der Sortierspalte wie die letzte zurückgegebene Zeile aufweisen, werden diese ohne WITH TIES ausgelassen.

Wenn Sie WITH TIES verwenden, schließt die Abfrage alle zusätzlichen Zeilen ein, die denselben Sortierungswert wie die letzte Zeile der durch die TOP-Klausel definierten Menge haben. Dies ist besonders nützlich, wenn Sie eine sortierte Liste zurückgeben und sicherstellen möchten, dass alle Zeilen mit gleichwertigen Sortierungsschlüsseln einbezogen werden.

```sql
SELECT TOP 10 WITH TIES
	Mitarbeiter_ID,
	Umsatz 
FROM [dbo].[tbl_Verkäufe]
ORDER BY Umsatz DESC
```

Nicht verstanden? Ich erkläre den Unterschied an Hand eines einfachen Beispieles: 

Stellen Sie sich eine Tabelle mit Testergebnissen vor. Sie möchten die Top-3-Ergebnisse aus dieser Tabelle abrufen. Wenn Sie einfach SELECT TOP 3 ... verwenden, bekommen Sie die drei besten Ergebnisse. Was aber, wenn es ein Unentschieden gibt? Was, wenn mehrere Schüler denselben dritten besten Punktestand haben? Wenn Sie nur TOP 3 verwenden, erhalten Sie möglicherweise nur einen dieser Schüler, obwohl es fair wäre, alle Schüler mit diesem Punktestand zu berücksichtigen.

Hier kommt WITH TIES ins Spiel. Wenn Sie SELECT TOP 3 WITH TIES ... verwenden, werden alle Schüler zurückgegeben, die in den Top 3 sind, plus alle weiteren Schüler, die denselben Punktestand haben wie der Schüler auf dem dritten Platz. Wenn also drei Schüler jeweils 95 Punkte und zwei weitere jeweils 94 Punkte haben, werden mit TOP 3 WITH TIES alle fünf Schüler zurückgegeben, da die zwei zusätzlichen Schüler denselben Punktestand wie der dritte Platz haben.

WITH TIES stellt sicher, dass alle Zeilen mit gleichwertigen Werten in der Sortierungsspalte einbezogen werden, und verhindert so, dass Zeilen mit denselben Werten wie die letzte der TOP-Auswahl zufällig ausgeschlossen werden.

**Wenn Sie TOP 10 WITH TIES in einer SQL-Abfrage verwenden, kann das Ergebnis mehr als 10 Zeilen enthalten**. Die WITH TIES-Klausel bewirkt, dass alle zusätzlichen Zeilen, die denselben Wert in der Sortierungsspalte haben wie die letzte der ersten 10 Zeilen, ebenfalls in das Ergebnis aufgenommen werden.
**Wenn Sie nur TOP 10 in einer SQL-Abfrage verwenden, werden exakt 10 Zeilen als Ergebnis erscheinen**, unabhängig davon, ob es noch weitere mit gleichem Ergebnis haben.

# PERCENT

Um einen Prozentsatz der in Frage kommenden Zeilen zurückzugeben, verwenden Sie die PERCENT-Option mit TOP anstelle einer festen Anzahl.

```sql
SELECT TOP 10 PERCENT
	Mitarbeiter_ID,
	Umsatz 
FROM [dbo].[tbl_Verkäufe]
ORDER BY Umsatz DESC
```

# Zusammenfassung
Die `TOP`-Option wird oft von SQL Server-Experten genutzt, um eine spezifische Anzahl von Zeilen aus einer Datenbank abzurufen. Wenn Sie die `TOP`-Option verwenden, sollten Sie jedoch einige wichtige Punkte beachten:

1. **Proprietäre T-SQL-Funktion**: `TOP` ist eine spezifische Erweiterung von T-SQL, der Sprache, die in Microsoft SQL Server verwendet wird. Sie ist nicht Teil des standardisierten SQL und kann daher in anderen Datenbanksystemen nicht verfügbar sein.

2. **Keine Unterstützung zum Überspringen von Zeilen**: Die `TOP`-Option alleine bietet keine Möglichkeit, eine bestimmte Anzahl von Zeilen zu überspringen. Sie gibt lediglich eine maximale Anzahl von Zeilen zurück, die an den Anfang der Ergebnismenge gesetzt werden.

3. **Abhängigkeit von der `ORDER BY`-Klausel**: Die `TOP`-Option hängt von der `ORDER BY`-Klausel ab, um die Zeilen zu bestimmen, die zurückgegeben werden. Sie können nicht eine Sortierreihenfolge nutzen, um die durch `TOP` gefilterten Zeilen auszuwählen, und gleichzeitig eine andere Reihenfolge für die Ausgabe der Ergebnisse festlegen. Die Sortierreihenfolge, die Sie für `TOP` verwenden, bestimmt also sowohl die Auswahl der Zeilen als auch ihre Reihenfolge in den Ergebnissen.