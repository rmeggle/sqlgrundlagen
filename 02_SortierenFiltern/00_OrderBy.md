# ORDER BY 

In SQL Server ist die `ORDER BY`-Klausel das letzte Element, das bei der Verarbeitung einer `SELECT`-Anweisung ausgeführt wird. Sie dient dazu, die Reihenfolge zu bestimmen, in der die Ergebnisse einer Abfrage zurückgegeben werden. Da SQL Server die physische Reihenfolge der Zeilen in einer Tabelle nicht garantiert, ist `ORDER BY` die einzige Methode, um die Reihenfolge der an den Client zurückgegebenen Ergebnisse festzulegen. Dies entspricht den Prinzipien der relationalen Datenbanktheorie.

**Anwendung der ORDER BY-Klausel:**

Um SQL Server anzuweisen, die Ergebnisse in einer bestimmten Reihenfolge zurückzugeben, fügen Sie die `ORDER BY`-Klausel wie folgt hinzu:

```sql
SELECT <select_list>
FROM <table_source>
ORDER BY <order_by_list> [ASC|DESC];
```

Mit `ORDER BY` können Sie verschiedene Arten von Elementen zur Sortierung verwenden:

- **Spaltennamen**: Sie können die Ergebnisse nach den angegebenen Spaltennamen sortieren. Die Sortierung erfolgt zunächst nach der ersten Spalte, dann nach den nachfolgenden Spalten.
- **Spaltenaliase**: Da `ORDER BY` nach der `SELECT`-Klausel ausgeführt wird, können Sie auf in der `SELECT`-Liste definierte Aliase zugreifen.
- **Ordinalposition der Spalten in der SELECT-Liste**: Die Verwendung der Position wird nicht empfohlen, da sie die Lesbarkeit verringert und eine fortlaufende Aktualisierung der `ORDER BY`-Liste erfordert. Bei komplexen Ausdrücken kann die Positionsnummer jedoch zur Problembehandlung nützlich sein.
- **Spalten, die nicht in der SELECT-Liste enthalten sind**: Diese können verwendet werden, solange sie in den Tabellen der `FROM`-Klausel vorhanden sind. Wenn die Abfrage `DISTINCT` verwendet, müssen alle Spalten der `ORDER BY`-Liste in der `SELECT`-Liste enthalten sein.

**Sortierrichtung:**

Neben der Auswahl der Spalten zur Sortierung können Sie auch die Sortierrichtung angeben. Verwenden Sie `ASC` für eine aufsteigende Sortierung (A bis Z, 0 bis 9) oder `DESC` für eine absteigende Sortierung (Z bis A, 9 bis 0). Die Standardrichtung ist aufsteigend. Sie können für jede Spalte eine eigene Richtung festlegen, wie im folgenden Beispiel gezeigt:

```sql
SELECT 
	Artikel,
	WG2_Bezeichnung,
	WG1_Bezeichnung
FROM 
[dbo].[tbl_Produkt]
ORDER BY 
	Artikel DESC,
	WG2_Bezeichnung ASC,
	WG1_Bezeichnung ASC
```

In diesem Beispiel werden die Produkte zuerst nach Artikel in absteigender Reihenfolge und dann nach WG-Bezeichnungen in aufsteigender Reihenfolge sortiert.