# Überblick

In diesem Kurs lernen Sie die Grundlagen von Transact-SQL (kurz TSQL), einer speziellen Version der SQL-Sprache, die von Microsoft entwickelt wurde. Sie erfahren, wie man Daten in relationalen Datenbanken abfragt und ändert. Diese Datenbanken laufen auf Microsoft SQL Server, Azure SQL-Datenbank und Azure Synapse Analytics. Die Inhalte dieser Schulung lässt sich daher auf viele Anwendungsgebiete übertragen.

# Zielgruppenprofil

Dieser Kurs ist für alle geeignet, die lernen möchten, wie man einfache SQL- oder Transact-SQL-Abfragen schreibt. Das umfasst Data Analysts, technische und wissenschaftliche Datenexperten, Datenbankadministratoren und Datenbankentwickler. Aber auch andere Personen, die sich mit Daten beschäftigen oder mehr über Datenverarbeitung lernen wollen, wie z.B. Lösungsarchitekten, Studierende und Führungskräfte im Technologiebereich, finden den Kurs hilfreich.

# Kurslehrplan

Sie können den Kurs entweder in einem Klassenraum mit einem Lehrer oder alleine durch Selbststudium absolvieren.

# Installation

Sie können die Datenbank, auf der sich alle Abfragen beziehen, durch das Ausführen des SQL-Scriptes, welches Sie unter "sqlgrundlagen\00_Schulungsdatenbank", finden auf einer SQL-Serverinstanz von Ihnen aus. 

1. Erstellen Sie eine Datenbank auf Ihrem SQL-Server
2. Wechseln Sie im SQL-Management-Studio in diese Datenbank (use <NAME DER DATENBANK>) 
3. Führen Sie das SQL-Scripte "Schulungsdatenbank.sql" aus. 

# Über die Schulungsdatenbank

Die Schulungsdatenbank enthält folgende Tabellen:

* tbl_Mitarbeiter
* tbl_Gehälter
* tbl_Produkt
* tbl_Raum
* tbl_Verkäufe

Bei der tbl_Mitarbeiter handelt es sich um eine sog. rekursive Tabelle, dessen Inhalte absichtlich nicht optimal gepflegt wurden. 
Der Inhalt der anderen Tabellen geht aus der Benennung derer hervor. 