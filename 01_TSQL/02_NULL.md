# NULL ist meist nichts

Ein "NULL"-Wert in einer Datenbank steht für die Abwesenheit eines Wertes oder für einen unbekannten Wert. Er repräsentiert nicht die Zahl Null, eine leere Zeichenkette oder "nichts". Stattdessen wird "NULL" eingesetzt, wenn ein Wert noch nicht vorhanden oder unbekannt ist, wie zum Beispiel, wenn ein Kunde noch keine E-Mail-Adresse angegeben hat. Manchmal geben Konvertierungsfunktionen einen "NULL"-Wert zurück, wenn sie auf einen Wert stoßen, der nicht in den gewünschten Datentyp umgewandelt werden kann.

NULL-Werte sind in relationalen Datenbanken Werte, die für eine Spalte unbekannt oder nicht vorhanden sind. Sie werden in der Regel durch einen leeren Platz dargestellt.

Bei der Erstellung einer Datenbank ist es wichtig, darauf zu achten, dass NULL-Werte so weit wie möglich vermieden werden. Dies hat folgende Gründe:

* NULL-Werte können zu Inkonsistenzen in den Daten führen. Wenn ein Wert in einer Spalte NULL ist, kann es nicht mit einem Wert in einer anderen Spalte verglichen werden. Dies kann zu Fehlern in Abfragen und Berechnungen führen.
* NULL-Werte können die Leistung von Abfragen beeinträchtigen. Wenn ein Abfrageergebnis NULL-Werte enthält, muss der Abfrageprozess diese Werte zunächst erkennen und behandeln. Dies kann die Ausführungszeit der Abfrage verlängern.
* NULL-Werte können die Datenanalyse erschweren. Wenn eine Spalte NULL-Werte enthält, kann es schwierig oder unmöglich sein, aus diesen Daten sinnvolle Erkenntnisse zu gewinnen.

Um NULL-Werte zu vermeiden, ist es wichtig, die Anforderungen an die Datenbanken sorgfältig zu analysieren. Wenn ein Wert in einer Spalte nicht erforderlich ist, sollte der Spaltentyp auf einen Datentyp festgelegt werden, der NULL-Werte nicht zulässt.

Wenn NULL-Werte in einer Spalte unvermeidlich sind, ist es wichtig, diese sorgfältig zu behandeln. Dies kann durch Verwendung von SQL-Befehlen wie ISNULL oder COALESCE erfolgen.

Allerdings gibt es Szenarien, in denen die Verwendung von NULL-Werten angemessen ist, insbesondere wenn das explizite Fehlen eines Wertes eine wichtige Information darstellt. In diesen Fällen ist es wichtig, dass die Bedeutung von NULL-Werten klar definiert und konsequent im gesamten Datenbankschema und in der Anwendungslogik gehandhabt wird.

Die folgenden SQL-Befehle können verwendet werden, um mit NULL-Werten leichter umzugehen:

* **ISNULL**: Diese Funktion gibt den angegebenen Wert zurück, wenn dieser nicht NULL ist. Andernfalls wird NULL zurückgegeben.
* **COALESCE**: Diese Funktion gibt den ersten nicht-NULL-Wert aus einer Liste von Werten zurück.
* **CASE**: Diese Funktion ermöglicht es, unterschiedliche Werte für NULL- und nicht-NULL-Werte zu definieren.

# ISNULL

Die Funktion "ISNULL" verwendet immer zwei Argumente. Das erste ist das Attribut (Spalte), die angezeigt werden soll. Sollte der Wert dieses ersten Arguments "NULL" sein, gibt die Funktion das zweite Argument zurück. Wenn der erste Ausdruck nicht "NULL" ist, wird er unverändert zurückgegeben. Im folgenden Beispiel werden alle NULL-Felder mit "N/A" angezeigt:

```sql 
SELECT 
	ISNULL(Mitarbeiter, 'N/A') as Name
FROM [dbo].[tbl_Mitarbeiter]
```

Es ist darauf zu achten, dass die hier ersetzten Werte immer den selben Datentyp haben muss. Ist die Spalte, wie in unserem Beispiel, ein NVARCAR, so muss eine Zeichenkette entsprechend als Alternative genannt werden. Bei einem INTEGER-Wert entsprechend eine Ganzzahl, etc. pp.

# COALECE

ISNULL ist kein Ansi-Standard. Die COALESCE-Funktion hingegen schon und ist in T-SQL ist ein nützliches Werkzeug, um mit NULL-Werten umzugehen. Sie ermöglicht es Ihnen, aus einer Liste von Werten den ersten nicht-NULL-Wert zurückzugeben. Sollten alle Werte in der Liste NULL sein, gibt die Funktion NULL zurück. COALESCE kann besonders hilfreich sein, um Standardwerte festzulegen oder NULL-Werte in Abfragen und Datenmanipulationen zu vermeiden.

```sql 
SELECT 
	COALESCE(Artikel, WG1_Bezeichnung, WG2_Bezeichnung, 'N/A') as Name
FROM [dbo].[tbl_Produkt]
```

# NULLIF

Entgegen aller bisherigen Funktionen, welche NULL in einen gültigen Wert umwandeln, so gibt es die Funktion NULLIF, welches eine NULL generiert, sollte ein Fall eingetreten sein: 

```sql 
SELECT 
      NULLIF(Umsatz, 0) AS Umsatz
FROM [dbo].[tbl_Verkäufe]
```