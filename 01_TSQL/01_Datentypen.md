# Datentypen in TSQL

In Transact-SQL, wie in anderen Programmiersprachen, haben sowohl Spalten in Datenbanktabellen als auch Variablen einen spezifischen Datentyp. Der Datentyp einer Spalte oder einer Variablen bestimmt, wie Werte in Ausdrücken behandelt werden. Beispielsweise können Sie mit dem Operator „+“ zwei Zeichenfolgen verketten oder zwei numerische Werte addieren. Die Art und Weise, wie dieser Operator funktioniert, hängt also vom Datentyp der verwendeten Werte ab.

Hier sind einige gängige Datentypen, die in einer SQL Server-Datenbank unterstützt werden:

| Datentyp  | Beschreibung                                   |
|-----------|------------------------------------------------|
| Int       | Ganzzahliger numerischer Datentyp              |
| VarChar   | Variabler Zeichenfolgentyp                     |
| DateTime  | Datums- und Zeitdatentyp                       |
| Float     | Numerischer Datentyp für Gleitkommazahlen      |
| Bit       | Boolescher Datentyp (0 oder 1)                 |
| Decimal   | Numerischer Datentyp für genaue Dezimalzahlen  |
| Char      | Fester Zeichenfolgentyp                        |
| NVarChar  | Variabler Unicode-Zeichenfolgentyp             |
| Blob/Binary | Datentypen für binäre Daten                  |

Darüber hinaus gibt es in TSQL noch weiterführende, speziellere Datentypen für spezielle Anwendungsgebiete: 

| Datentyp    | Beschreibung                                        |
|-------------|-----------------------------------------------------|
| Money       | Numerischer Datentyp für Währungswerte              |
| Decimal     | Numerischer Datentyp für genaue Dezimalzahlen       |
| Image       | Datentyp für die Speicherung von großen Bildern     |
| SmallInt    | Kleinerer ganzzahliger numerischer Datentyp         |
| BigInt      | Großer ganzzahliger numerischer Datentyp            |
| Real        | Numerischer Datentyp für einfache Gleitkommazahlen  |
| Date        | Datentyp nur für Datum                              |
| Time        | Datentyp nur für Uhrzeit                            |
| DateTime2   | Erweiterter Datentyp für Datum und Uhrzeit          |
| SmallMoney  | Kleinerer numerischer Datentyp für Währungswerte    |
| UniqueIdentifier | Datentyp für eindeutige Identifikatoren         |
| VarBinary   | Datentyp für variable binäre Daten                  |
| Xml         | Datentyp für XML-Daten                              |
| NChar       | Fester Unicode-Zeichenfolgentyp                     |
| NText       | Datentyp für lange Unicode-Textdaten                |

Jeder dieser Datentypen hat spezifische Eigenschaften und wird je nach den Anforderungen der Daten und der gewünschten Operationen in der Datenbank verwendet. Es ist wichtig, den richtigen Datentyp für jede Spalte in Ihrer Datenbank auszuwählen, um die Effizienz und Genauigkeit der Datenmanipulation und -speicherung zu gewährleisten.

# Konvertierung von Datentyen

Die Konvertierung von Datentypen ist ein wichtiges Konzept in T-SQL, insbesondere wenn Sie mit verschiedenen Arten von Daten arbeiten. Es gibt zwei Arten der Typkonvertierung: implizit und explizit.

**Implizite Konvertierung**: Dies geschieht automatisch, wenn Werte von einem Datentyp in einen anderen umgewandelt werden müssen und die Konvertierung ohne Datenverlust oder -verfälschung möglich ist. Ein Beispiel hierfür ist die Addition eines Integers (ganze Zahl) zu einem Float (Gleitkommazahl). T-SQL handhabt diese Konvertierung automatisch.

**Explizite Konvertierung**: Manchmal erfordern Konvertierungen zwischen inkompatiblen Datentypen eine explizite Anweisung, um Fehler zu vermeiden. Zum Beispiel, wenn Sie versuchen, einen `varchar`-Wert mit einem `decimal`-Wert mit dem `+`-Operator zu verketten, wird dies nicht automatisch funktionieren, da es sich um zwei völlig unterschiedliche Datentypen handelt. In diesem Fall müssen Sie explizit den `decimal`-Wert in einen `varchar` konvertieren, bevor Sie die Verkettung durchführen können. Dies kann mit Funktionen wie `CAST` oder `CONVERT` in T-SQL erreicht werden.

Hier ist ein Beispiel für eine explizite Konvertierung:

```sql
-- Angenommen, @decimalValue ist ein decimal-Wert und @varcharValue ist ein varchar-Wert
SELECT CAST(@decimalValue AS VARCHAR(10)) + @varcharValue
```

In diesem Beispiel wird der `decimal`-Wert zuerst in einen `varchar`-Typ konvertiert, sodass er dann sicher mit einem anderen `varchar`-Wert verkettet werden kann. Explizite Konvertierungen sind besonders wichtig, wenn die implizite Konvertierung zu Datenverlust oder Fehlern führen könnte.

Im folgenden Beispiel, etwas konstruiert wird eine Abfrage die Menge durch die Umsätze dividieren um einen Schnitt zu ermitteln, wie hoch der Verkaufserfolg eines Mitarbeiters war: 

```sql
SELECT [Menge]
      ,[Umsatz]
	  ,CAST([Menge] / [Umsatz] AS Float)
  FROM [dbo].[tbl_Verkäufe]
WHERE Mitarbeiter_ID=1
```

Bitte beachten Sie, dass der Spaltentyp "Menge" ein SMALLINT ist, und Umsatz der Datentyp "Money", das Ergebnis dieser Division soll jedoch als Gleitkommazahl dargestellt werden. 

## "CAST" und "TRY_CAST" genauer betrachtet

Die `CAST`-Funktion in T-SQL wird verwendet, um einen Wert explizit von einem Datentyp in einen anderen zu konvertieren, vorausgesetzt, die Konvertierung ist möglich und sinnvoll. Wenn die Konvertierung nicht durchgeführt werden kann, weil die Datentypen inkompatibel sind, gibt die Funktion einen Fehler zurück.

Hier ist ein Beispiel, wie Sie `CAST` verwenden können, um ganzzahlige Werte einer Spalte in `varchar`-Werte zu konvertieren, damit diese mit einem anderen zeichenbasierten Wert verkettet werden können:

```sql
SELECT 
      CAST([WG2_Bezeichnung] AS VarChar(50)) + ': ' + CAST([Artikel] AS varchar(10))
FROM [schulungsdatenbank].[dbo].[tbl_Produkt]
```

In diesem Beispiel:

- `WG2_Bezeichnung` ist eine Spalte mit möglichen UNICODE-Zeichen).
- `CAST([WG2_Bezeichnung] AS VarChar(50))` konvertiert jede `WG2_Bezeichnung` in einen `varchar`-Wert mit maximal 50 Zeichen.
- Dann wird dieser `varchar`-Wert mit dem String `': '` verkettet.
- Anschließend mit `Artikel`, der ebenfalls von möglichen UNICODE-Zeichen bereinigt wurde, verkettet. 

Diese Art der Konvertierung ist auch nützlich, wenn Sie numerische Daten in einem Format darstellen müssen, das für die Verkettung mit Text geeignet ist.

Gerade ist das Umwandeln von UNICODE-Zeichen heute ein sicherliche größeres Thema. Sollte die Abfrage, welche Sie mittels CAST von Unicode bereinigt haben wollen, dann doch zu folgender Fehlermeldung führen: 

> Fehler: „Fehler beim Konvertieren des nvarchar-Werts ‚M‘ in den Datentyp ‚int‘.“

Ist *mindestens* ein Wert in der Ergebnismenge nicht zu überführen und führt zu einem Fehler. 

In einigen Fällen ist es jedoch besser, wenn in jedem Fall ein Ergebnis geliefert, aber eventuell nicht überführbare Zellen mittels *NULL* gekennzeichnet werden. Hiefür dient der Befehl TRY_CAST:

```sql
SELECT 
      TRY_CAST([WG2_Bezeichnung] AS VarChar(50)) + ': ' + TRY_CAST([Artikel] AS varchar(10))
FROM [schulungsdatenbank].[dbo].[tbl_Produkt]
```
Die Wahl zwischen `CAST` und `TRY_CAST` in T-SQL hängt davon ab, wie Sie mit Konvertierungsfehlern umgehen möchten.

1. **CAST**: 
   - `CAST` wird verwendet, um einen Wert explizit von einem Datentyp in einen anderen zu konvertieren.
   - Wenn die Konvertierung erfolgreich ist, gibt `CAST` den konvertierten Wert zurück.
   - Wenn die Konvertierung jedoch nicht möglich ist (z.B. beim Versuch, einen Text, der nicht in eine Zahl umgewandelt werden kann, in einen numerischen Datentyp zu konvertieren), führt `CAST` zu einem Fehler. 
   - Sie würden `CAST` verwenden, wenn Sie sicher sind, dass die Konvertierung in den meisten Fällen erfolgreich sein wird oder wenn es wichtig ist, dass ein Fehler ausgelöst wird, falls die Konvertierung scheitert.

2. **TRY_CAST**:
   - `TRY_CAST` funktioniert ähnlich wie `CAST`, bietet jedoch eine eingebaute Fehlerbehandlung.
   - Wenn die Konvertierung erfolgreich ist, gibt `TRY_CAST` den konvertierten Wert zurück, genau wie `CAST`.
   - Wenn die Konvertierung jedoch nicht möglich ist, gibt `TRY_CAST` anstelle eines Fehlers `NULL` zurück.
   - `TRY_CAST` ist nützlich in Situationen, in denen Sie die Abfrage auch bei potenziell fehlerhaften Konvertierungen fortsetzen möchten, ohne dass die Abfrage durch einen Fehler unterbrochen wird.

**Zusammenfassend**: Verwenden Sie `CAST`, wenn Sie sicher sind, dass die Konvertierung erfolgreich sein wird, oder wenn ein Konvertierungsfehler ein wichtiges Signal für ein Problem ist. Verwenden Sie `TRY_CAST`, wenn Sie möchten, dass Ihre Abfrage auch dann weiterläuft, wenn einige Werte nicht konvertiert werden können, und Sie in solchen Fällen mit `NULL` als Rückgabewert arbeiten können.


# CONVERT und TRY_CONVERT

CAST ist die SQL-Funktion (nach ANSI-Standard) zum Konvertieren zwischen Datentypen und wird in vielen Datenbanksystemen verwendet. In Transact-SQL können Sie auch die Funktion CONVERT verwenden.

Die Wahl zwischen `CAST` und `CONVERT` in T-SQL hängt von den spezifischen Anforderungen Ihrer Datenkonvertierung und Ihren Vorlieben in Bezug auf die Syntax ab.

1. **CAST**:
   - `CAST` ist ein Teil des SQL-Standards und somit in vielen SQL-Datenbankmanagementsystemen verfügbar.
   - Die Syntax ist unkompliziert und sieht folgendermaßen aus: `CAST(Ausdruck AS Datentyp)`.
   - `CAST` ist nützlich für grundlegende Konvertierungen von einem Datentyp in einen anderen, wenn keine zusätzlichen Formatierungsoptionen benötigt werden.

2. **CONVERT**:
   - `CONVERT` ist spezifisch für SQL Server und bietet zusätzliche Funktionalitäten im Vergleich zu `CAST`.
   - Die Syntax von `CONVERT` erlaubt nicht nur die Angabe des Zieldatentyps, sondern auch eines optionalen Stilparameters, der besonders nützlich ist, wenn man mit Datums- und Zeitdatentypen arbeitet. Die Syntax sieht so aus: `CONVERT(Datentyp, Ausdruck, [Stil])`.
   - `CONVERT` ist ideal, wenn Sie zusätzliche Kontrolle über das Formatierungsergebnis benötigen, besonders bei Datums- und Zeitkonvertierungen. Zum Beispiel können Sie das Format des Datums angeben, das Sie zurückbekommen möchten.

**Zusammenfassend**: Verwenden Sie `CAST`, wenn Sie eine einfache, standardkonforme Konvertierung benötigen. Greifen Sie auf `CONVERT` zurück, wenn Sie speziellere Konvertierungen durchführen möchten, insbesondere wenn Sie zusätzliche Formatierungsoptionen für Datums- und Zeitdatentypen benötigen.


Beispiel: 

```sql
SELECT 
	Zeit_ID 
FROM tbl_Gehälter
```

Liefert Datumswerte in folgender Form: 2022-01-01 00:00:00.000
Um das Datum 2022-01-01 00:00:00.000 in das Format Tag.Monat.Jahr umzuwandeln, können Sie die CONVERT-Funktion in T-SQL mit einem benutzerdefinierten Format verwenden. Da T-SQL keine direkte Formatoption für Tag.Monat.Jahr bietet, müssen Sie eine Kombination aus CONVERT und String-Funktionen verwenden, um das gewünschte Format zu erreichen.


```sql
SELECT 
	CONVERT(VARCHAR, Zeit_ID, 104) 
FROM tbl_Gehälter
```

Die CONVERT-Funktion in T-SQL unterstützt verschiedene Formatierungscodes, um Datums- und Zeitwerte in unterschiedliche Darstellungsformen zu konvertieren. Hier ist eine Tabelle der gängigen Formatierungscodes für Datumskonvertierungen:

| Code | Format                                      | Beispiel                   |
|------|---------------------------------------------|----------------------------|
| 100  | Mon DD YYYY HH:MIAM (oder PM)               | Jan 01 2022 12:00AM        |
| 101  | mm/dd/yyyy                                  | 01/01/2022                 |
| 102  | yyyy.mm.dd                                  | 2022.01.01                 |
| 103  | dd/mm/yyyy                                  | 01/01/2022                 |
| 104  | dd.mm.yyyy                                  | 01.01.2022                 |
| 105  | dd-mm-yyyy                                  | 01-01-2022                 |
| 106  | dd Mon yyyy                                 | 01 Jan 2022                |
| 107  | Mon dd, yyyy                                | Jan 01, 2022               |
| 108  | HH:MI:SS                                    | 00:00:00                   |
| 109  | Mon DD YYYY HH:MI:SS:MMMAM (oder PM)        | Jan 01 2022 12:00:00:000AM |
| 110  | mm-dd-yyyy                                  | 01-01-2022                 |
| 111  | yyyy/mm/dd                                  | 2022/01/01                 |
| 112  | yyyymmdd                                    | 20220101                   |
| 113  | dd Mon yyyy HH:MI:SS:MMM (24h)              | 01 Jan 2022 00:00:00:000   |
| 114  | HH:MI:SS:MMM(24h)                           | 00:00:00:000               |
| 120  | yyyy-mm-dd HH:MI:SS (24h)                   | 2022-01-01 00:00:00        |
| 121  | yyyy-mm-dd HH:MI:SS.MMM (24h)               | 2022-01-01 00:00:00.000    |
| 126  | yyyy-mm-ddTHH:MI:SS.MMM (ISO8601)           | 2022-01-01T00:00:00.000    |
| 127  | yyyy-mm-ddTHH:MI:SS.MMMZ (ISO8601 mit TZ)   | 2022-01-01T00:00:00.000Z   |
| 130  | dd Mon yyyy HH:MI:SS:MMMAM (arabisch)       | 01 Jan 2022 12:00:00:000AM |
| 131  | yyyy/mm/dd HH:MI:SS:MMMAM (arabisch)        | 2022/01/01 12:00:00:000AM  |


# PARSE und TRY_PARSE
In T-SQL werden die Funktionen `PARSE` und `TRY_PARSE` verwendet, um Zeichenfolgen in einen bestimmten Datentyp zu konvertieren, ähnlich wie `CAST` und `CONVERT`, bieten jedoch zusätzliche Funktionalitäten im Hinblick auf die Formatierung und sind insbesondere nützlich bei der Konvertierung von Datums- und Zeitangaben sowie Zahlenwerten. 

1. **PARSE**:
   - `PARSE` wird verwendet, um eine Zeichenfolge in einen anderen Datentyp zu konvertieren, normalerweise Datums-, Zeit- oder Zahlentypen. Es erfordert die Angabe eines Kulturformats.
   - Beispiel: Konvertieren eines Datumsstrings in das `DateTime`-Format.
     ```sql
     SELECT PARSE('1 January 2022' AS datetime USING 'en-US') AS Result;
     ```
   - In diesem Beispiel wird der String '1 January 2022' in ein `DateTime`-Objekt konvertiert, wobei das englische (US) Format für das Datum verwendet wird.

2. **TRY_PARSE**:
   - `TRY_PARSE` bietet dieselbe Funktionalität wie `PARSE`, fügt aber eine Fehlerbehandlung hinzu. Wenn die Konvertierung fehlschlägt, gibt `TRY_PARSE` statt eines Fehlers `NULL` zurück.
   - Beispiel: Versuch, einen fehlerhaft formatierten Datumswert zu konvertieren.
     ```sql
     SELECT TRY_PARSE('NotADate' AS datetime USING 'en-US') AS Result;
     ```
   - Hier versucht `TRY_PARSE`, den String 'NotADate' in ein `DateTime`-Objekt zu konvertieren. Da der String kein gültiges Datum ist, gibt die Funktion `NULL` zurück, anstatt einen Fehler auszulösen.

Beide Funktionen sind sehr nützlich für Situationen, in denen Daten aus unterschiedlichen Formaten oder Kulturen konvertiert werden müssen und eine strikte Typensicherheit erforderlich ist. `TRY_PARSE` bietet zusätzliche Sicherheit, da es in Situationen, in denen die Konvertierung nicht möglich ist, eine Ausnahme verhindert.

Hier ist eine Tabelle einiger häufig verwendeter Länder- und Kulturcodes für die Verwendung mit PARSE und TRY_PARSE in T-SQL:

| Sprache    | Land/Region    | Kulturcode |
|------------|----------------|------------|
| Deutsch    | Deutschland    | de-DE      |
| Deutsch    | Österreich     | de-AT      |
| Deutsch    | Schweiz        | de-CH      |
| Englisch   | USA            | en-US      |
| Englisch   | Großbritannien | en-GB      |
| Englisch   | Australien     | en-AU      |
| Englisch   | Kanada         | en-CA      |
| Englisch   | Indien         | en-IN      |
| Französisch| Frankreich     | fr-FR      |
| Französisch| Kanada         | fr-CA      |
| Französisch| Schweiz        | fr-CH      |
| Französisch| Belgien        | fr-BE      |
| Spanisch   | Spanien        | es-ES      |
| Spanisch   | Mexiko         | es-MX      |
| Spanisch   | Argentinien    | es-AR      |
| Spanisch   | Kolumbien      | es-CO      |
| Italienisch| Italien        | it-IT      |
| Italienisch| Schweiz        | it-CH      |


# STR

Die Funktion "STR" konvertiert schlicht einen numerischen Wert in einen varchar-Wert.

```sql
SELECT 
	STR(Umsatz) 
FROM tbl_Verkäufe
```

Dies funktioniert allerdings nur problemlos bei der Konvertierung von z.B. INT, FLOAT. Versucht man hier allerdings eine Datetime-Spalte zu konvertieren, so kann dies durchaus mit der frustierenden Fehlermeldung quittiert werden. Zum Beispiel: 

> Die implizite Konvertierung vom datetime-Datentyp in den float-Datentyp in der tbl_Gehälter-Tabelle, Zeit_ID-Spalte ist nicht zulässig. Verwenden Sie die CONVERT-Funktion, um diese Abfrage auszuführen.

