
# SELECT

Transact-SQL, oft als T-SQL bezeichnet, ist eine spezielle Version der SQL-Sprache, die den ANSI-Standard befolgt und in Microsofts SQL-Produkten und -Dienstleistungen zum Einsatz kommt. Sie ist dem Standard-SQL sehr ähnlich. In diesem Kurs konzentrieren wir uns hauptsächlich auf die SELECT-Anweisung, die bei weitem die meisten Optionen und Varianten unter den DML-Anweisungen (Data Manipulation Language) bietet.

Beginnen wir damit, zu verstehen, wie eine SELECT-Anweisung verarbeitet wird. Wichtig ist zu wissen, dass die Reihenfolge, in der wir eine SELECT-Anweisung schreiben, nicht der Reihenfolge entspricht, in der die SQL Server-Datenbank-Engine diese Anweisung auswertet und verarbeitet. Diese Unterscheidung ist entscheidend für das Verständnis, wie Abfragen in SQL Server funktionieren und wie sie optimiert werden können.

## Einfaches Beispiel mit Erklärung

```sql
SELECT 
	WG2_Bezeichnung, 
	COUNT(*) as Anzahl
FROM [dbo].[tbl_Produkt]
WHERE WG1_Bezeichnung='Wein' 
GROUP BY WG2_Bezeichnung
HAVING COUNT(WG1_Bezeichnung) > 20;
```

Die Abfrage, die wir betrachten, besteht aus einer SELECT-Anweisung, die sich aus verschiedenen Teilen, den sogenannten Klauseln, zusammensetzt. Jede dieser Klauseln hat die Aufgabe, einen spezifischen Vorgang auf die abgerufenen Daten anzuwenden. Bevor wir uns genauer mit der Reihenfolge der Ausführung dieser Vorgänge beschäftigen, schauen wir uns kurz an, was diese spezielle Abfrage leisten soll. Wir werden jedoch in diesem Abschnitt nicht auf die Einzelheiten jeder Klausel eingehen.

In der SELECT-Klausel der Abfrage werden zwei Dinge festgelegt: Erstens wird die Spalte "WG2_Bezeichnung" ausgewählt und zurückgegeben, und zweitens wird die Anzahl der Werte zurückgegeben. Diese Anzahl wird mit dem Namen oder Alias "Anzahl" bezeichnet.

Untersuchen wir den Aufbau dieser Abfrage nun genauer: 

```sql
SELECT 
	* 
FROM [dbo].[tbl_Produkt];
```

Dies ist eine einfache Abfrage auf die Tabelle tbl_Produkt. Der Asterisk "*" gibt an, dass alle Spalten ausgegeben werden. Das ist in aller Regel immer ungünstig, besser ist es, genau anzugeben, welche Spalte(n) in der Ergebnismenge angezeigt werden: 

```sql
SELECT 
	WG2_Bezeichnung  
FROM [dbo].[tbl_Produkt];
```

Der Aufbau einer jeden Abfrage ist immer identisch. Schauen wir einmal in die Dokumentation von TSQL selbst hinein: 

```sql
SELECT [ALL | DISTINCT]  
   [Alias.] Select_Item [AS Column_Name]  
   [, [Alias.] Select_Item [AS Column_Name] ...]   
FROM [DatabaseName!]Table [Local_Alias]  
   [, [DatabaseName!]Table [Local_Alias] ...]   
[WHERE JoinCondition [AND JoinCondition  
...]  
   [AND | OR FilterCondition [AND | OR FilterCondition ...]]]  
[GROUP BY GroupColumn [, GroupColumn ...]]  
[HAVING FilterCondition]  
[UNION [ALL] SELECTCommand]  
[ORDER BY Order_Item [ASC | DESC] [, Order_Item [ASC | DESC] ...]]
```

Aus der Definition geht hervor, dass ein SELECT immer mit genau diesen Schlüsselwort, genannt an erster Stelle, eingeleitet wird. 
Anschließend können optional die Befehle ALL oder DISTINCT verwendet werden. Das Pipe-Zeichen "|" steht für "oder" und die eckigen Klammern für "optional". 

Nach Angabe der Spalten muss der Name der Tabelle genannt sein, aus der die Daten angezeigt werden sollen: 

```sql 
FROM [dbo].[tbl_Produkt];
```

Nach dem "FROM"-Schlüsselwort kann das "WHERE" folgen (beachten Sie bite hier die optionale Angabe durch die eckige Klammer in der Definition ):

```sql 
WHERE WG1_Bezeichnung='Wein';
``` 

Die nun folgende GROUP BY-Klausel in unserer Abfrage spielt eine wichtige Rolle. Nachdem die Daten durch eventuelle Filterbedingungen, die in anderen Teilen der Abfrage festgelegt sind, selektiert wurden, kommt die GROUP BY-Klausel zum Einsatz. Ihr Zweck ist es, die ausgewählten Zeilen anhand der "WG2_Bezeichnung" zu gruppieren. Das bedeutet, dass alle Zeilen, die die gleiche "WG2_Bezeichnung" haben, als eine einzige Gruppe behandelt werden. Für jede dieser Gruppen wird dann eine einzelne Zeile in den Ergebnissen der Abfrage zurückgegeben. 

Diese Gruppierungsfunktion ist entscheidend, um Zusammenfassungen oder Aggregationen wie Zählungen, Durchschnitte oder Summen für jede Gruppe zu berechnen. In unserem speziellen Fall hilft die GROUP BY-Klausel dabei, die Anzahl der Produkte (tbl_Produkt) pro Hauptbezeichnung (WG2_Bezeichnung) zu konsolidieren.

```sql 
GROUP BY WG2_Bezeichnung
``` 

Nachdem die Gruppen durch die GROUP BY-Klausel gebildet wurden, kommt die HAVING-Klausel ins Spiel. Diese Klausel funktioniert ähnlich wie die WHERE-Klausel, aber mit einem entscheidenden Unterschied: Während die WHERE-Klausel die Daten vor der Gruppierung filtert, wird die HAVING-Klausel verwendet, um die bereits gebildeten Gruppen zu filtern.

In unserer spezifischen Abfrage wird die HAVING-Klausel dazu verwendet, nur die Gruppen in das Endergebnis aufzunehmen, die einem bestimmten Kriterium entsprechen. In diesem Fall bezieht sich das Kriterium darauf, dass die Bezeichnung (WG2_Bezeichnung) mehr als 20 Warengruppen haben muss. Das bedeutet, dass die HAVING-Klausel alle Waren ausschließt, welche weniger 20 Stück im Lager sind, und nur diejenigen Datumsangaben in die Ergebnisse einbezieht, an denen mehr als 20 vorrätig sind. 

Diese Funktion der HAVING-Klausel ist besonders nützlich für die Analyse von Aggregatdaten, wie zum Beispiel das Identifizieren von Tagen mit hoher Bestellaktivität.

```sql 
HAVING COUNT(WG1_Bezeichnung) > 20;
``` 

# Die Angabe der Spalten im SELCT

Die Verwendung einer expliziten Spaltenliste in einer SELECT-Anweisung ermöglicht es Ihnen, genau zu bestimmen, welche Spalten und in welcher Reihenfolge diese in den Ergebnissen Ihrer Abfrage erscheinen. Wenn Sie die Spalten explizit in Ihrer SELECT-Anweisung auflisten, können Sie die Ausgabe Ihrer Abfrage präzise kontrollieren.

In den Ergebnissen wird jede ausgewählte Spalte mit dem Namen dieser Spalte als Überschrift angezeigt. Das bedeutet, dass die Kopfzeilen der Ergebnistabelle genau den Namen der in der SELECT-Anweisung aufgeführten Spalten entsprechen. So wird zum Beispiel, wenn Sie eine Spalte namens "Kundenname" in Ihrer SELECT-Anweisung haben, auch in der Ergebnistabelle eine entsprechende Kopfzeile "Kundenname" erscheinen.

Diese Methode der spezifischen Spaltenauswahl ist besonders nützlich, wenn Sie nur bestimmte Informationen aus einer umfangreichen Tabelle benötigen oder wenn Sie die Reihenfolge der Spalten in Ihrem Ergebnisset anpassen möchten.

```sql 
SELECT
	Artikel,
	WG2_Bezeichnung,
	WG1_Bezeichnung
FROM [dbo].[tbl_Produkt]
``` 

Selbstverständlich kann nun auch die Reihenfolge der Spalten geändert werden:

```sql 
SELECT
	WG1_Bezeichnung,
	WG2_Bezeichnung,
    Artikel
FROM [dbo].[tbl_Produkt]
``` 

Oder mittels einem "Alias" in eine neue Überschrift umbenannt werden: 

```sql 
SELECT
	Artikel,
	WG2_Bezeichnung as Untergruppe,
	WG1_Bezeichnung as Hauptgruppe
FROM [dbo].[tbl_Produkt]
``` 

# Zusammenfassung

Beim Formatieren von SQL-Abfragen gibt es einige Freiheiten, aber auch bewährte Praktiken, die helfen, den Code klar und leicht verständlich zu halten. Hier sind einige Richtlinien für das Schreiben von T-SQL-Code:

1. **Groß-/Kleinschreibung von Schlüsselwörtern**: Es ist üblich, T-SQL-Schlüsselwörter wie „SELECT“, „FROM“, „AS“ und ähnliche in Großbuchstaben zu schreiben. Diese Konvention hilft, Schlüsselwörter in komplexen Abfragen schnell zu identifizieren und macht den Code übersichtlicher.

2. **Neue Zeile für jede Hauptklausel**: Beginnen Sie jede Hauptklausel einer Anweisung in einer neuen Zeile. Das macht den Code strukturierter und verbessert die Lesbarkeit, besonders bei längeren Abfragen.

3. **Separate Zeilen für Elemente in der SELECT-Liste**: Wenn Ihre SELECT-Liste mehr als ein paar Spalten, Ausdrücke oder Aliase umfasst, ist es ratsam, jedes Element in einer eigenen Zeile aufzuführen. Dies trägt zur Übersichtlichkeit bei, besonders wenn die Abfrage komplexe Berechnungen oder mehrere Spalten umfasst.

4. **Einrückungen verwenden**: Verwenden Sie Einrückungen für Zeilen, die Unterklauseln oder Spalten beinhalten. Dies hilft zu verdeutlichen, welche Teile des Codes zu welcher Hauptklausel gehören. Einrückungen machen es einfacher, die Struktur und den Ablauf der Abfrage nachzuvollziehen.

Diese Praktiken sind besonders wichtig, wenn Sie mit komplexen oder langen Abfragen arbeiten. Sie helfen dabei, den Code sauber, organisiert und wartungsfreundlich zu halten, was die Fehlersuche (Debugging) und das Verständnis der Abfrage erleichtert.