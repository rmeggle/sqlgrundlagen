--
-- Die komplette Abfrage: 
--

SELECT 
	WG2_Bezeichnung, 
	COUNT(*) as Anzahl
FROM [dbo].[tbl_Produkt]
WHERE WG1_Bezeichnung='Wein' 
GROUP BY WG2_Bezeichnung
HAVING COUNT(WG1_Bezeichnung) > 20;

-- ----------------------------------------------
-- Aufbau der Abfrage
--

--
-- Select auf alle Spalten
--
SELECT 
	* 
FROM [dbo].[tbl_Produkt];

--
-- Select auf nur eine Spalte
--
SELECT 
	WG2_Bezeichnung 
FROM [dbo].[tbl_Produkt];


--
-- Select mit Filter "WHERE" f�r nur eine Produktbezeichnung
--
SELECT 
	WG2_Bezeichnung 
FROM [dbo].[tbl_Produkt] 
WHERE WG1_Bezeichnung='Wein';

--
-- Zusammenfassen aller Ergebnisse (eliminierung der doppelten Werte)
--
SELECT 
	WG2_Bezeichnung 
FROM [dbo].[tbl_Produkt] 
WHERE WG1_Bezeichnung='Wein'
GROUP BY WG2_Bezeichnung;

--
-- Zusammenfassen aller Ergebnisse (Z�hlen der doppelten Werte)
--
SELECT 
	WG2_Bezeichnung,
	count(*) 
FROM [dbo].[tbl_Produkt] 
WHERE WG1_Bezeichnung='Wein'
GROUP BY WG2_Bezeichnung;


--
-- Benennen der Zusammenfassung (diese Spalte ist errechnet, trug daher eine Willk�hrliche Bezeichnung)
--
SELECT 
	WG2_Bezeichnung, 
	count(*) as Anzahl
FROM [dbo].[tbl_Produkt] 
WHERE WG1_Bezeichnung='Wein'
GROUP BY WG2_Bezeichnung;

--
-- Anwenden eines Filters auf die gruppierten Elemente
--
SELECT 
	WG2_Bezeichnung, 
	count(*) as Anzahl
FROM [dbo].[tbl_Produkt] 
WHERE WG1_Bezeichnung='Wein'
GROUP BY WG2_Bezeichnung
HAVING COUNT(WG1_Bezeichnung) > 20;

--
-- Bennenung von Spalten und Verwenden von Alias
--
SELECT
	Artikel,
	WG2_Bezeichnung as Untergruppe,
	WG1_Bezeichnung as Hauptgruppe
FROM [dbo].[tbl_Produkt]

