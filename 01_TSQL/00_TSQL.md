# Einführung: SQL-Sprache

SQL steht für "Structured Query Language" (strukturierte Abfragesprache) und wird benutzt, um mit relationalen Datenbanken zu kommunizieren. Mit SQL-Befehlen kann man Daten aktualisieren oder aus der Datenbank abrufen. Zum Beispiel verwendet man den Befehl SELECT, um Daten zu erfragen. SQL wird in vielen Datenbanksystemen wie Microsoft SQL Server, MySQL und Oracle verwendet. Jeder Hersteller fügt zu dem Standard-SQL eigene Besonderheiten hinzu.

Lernziele:

- Das Verständnis von SQL und dessen Anwendung.
- Das Identifizieren von Datenbankobjekten.
- Das Kennenlernen verschiedener SQL-Anweisungen.
- Das Abfragen von Tabellen mit SELECT.
- Das Arbeiten mit verschiedenen Datentypen.
- Das Umgehen mit "NULL"-Werten.

# Transact-SQL

Grundlegende SQL-Anweisungen wie SELECT, INSERT, UPDATE und DELETE sind in allen relationalen Datenbanksystemen verfügbar. Viele Datenbanksysteme haben zusätzlich eigene Erweiterungen, die über den Standard hinausgehen, z.B. in Sicherheitsverwaltung und Programmierbarkeit. Microsoft-Datenbanksysteme benutzen den SQL-Dialekt "Transact-SQL" (T-SQL), der spezielle Erweiterungen für gespeicherte Prozeduren und Funktionen sowie Benutzerverwaltung enthält. 

SQL ist eine deklarative Sprache, was bedeutet, dass man mit ihr beschreibt, was man erreichen möchte, und die Datenbank entscheidet, wie sie das Ergebnis liefert. Im Gegensatz dazu definieren prozedurale Sprachen eine genaue Abfolge von Schritten, die der Computer ausführen muss.

# Relationale Daten

SQL wird meist für relationale Datenbanken verwendet. In solchen Datenbanken sind Daten in Tabellen organisiert, die jeweils eine Art von Daten (wie Kunden, Produkte oder Bestellungen) repräsentieren. Jede Zeile in einer Tabelle stellt eine Instanz dieser Daten dar (z.B. einen bestimmten Kunden). Die Tabellen sind über Schlüsselspalten miteinander verbunden, die jede Instanz eindeutig identifizieren.

# Satzbasierte Verarbeitung

Relationale Datenbanken basieren auf der Mengentheorie. Es ist wichtig, sich auf ganze Datensätze zu konzentrieren, anstatt nur auf einzelne Zeilen. In der Mengentheorie gibt es keine festgelegte Reihenfolge der Elemente. Das gilt auch für Tabellen in relationalen Datenbanken. Wenn Sie eine bestimmte Reihenfolge in den Ergebnissen benötigen, müssen Sie dies in Ihrer SQL-Abfrage explizit mit einer ORDER BY-Klausel angeben.

# Was ist ein "Schema"?

In SQL Server-Datenbanksystemen werden Tabellen innerhalb von Schemas erstellt, um unterschiedliche logische Bereiche innerhalb einer einzelnen Datenbank zu definieren. Diese Schemas sind nicht mit dem Namen der Datenbank selbst zu verwechseln. 

Zum Beispiel könnte die Tabelle der Mitarbeiter (tbl_Mitarbeiter) in einem Schema namens "HR" definiert werden, während eine Tabelle für Produkte (tbl_Produkt) in einem anderen Schema namens "Produktion" angelegt wird. 

Es ist möglich, dass in einer Datenbank, die Kundenbestellungen (tbl_Verkäufe) verfolgt im "Sales"-Schema existiert. Gleichzeitig könnte es eine Tabelle mit demselben Namen im "Production"-Schema geben, die Bestellungen von Lieferanten für Produktkomponenten nachverfolgt.

Ein Schema namens "Sales", das die Tabellen "tbl_Verkäufe" und "tbl_Mitarbeiter" enthält, und ein Schema namens "Production", das die Tabellen "tbl_Verkäufe" und "tbl_Produkt" enthält, sind somit unterschiedliche, voneinander getrennte Bereiche innerhalb derselben Datenbank.

SQL Server nutzt ein hierarchisches Benennungssystem für die Organisation seiner Datenstrukturen. Dieses System ermöglicht es, zwischen Tabellen mit demselben Namen in verschiedenen Schemas zu unterscheiden, da sie in unterschiedlichen logischen Bereichen der gleichen Datenbank liegen. Der vollständige Name eines Objekts umfasst die Instanz des Datenbankservers, auf dem die Datenbank gespeichert ist, den Namen der Datenbank selbst, den Namen des Schemas und den Namen der Tabelle. Ein Beispiel wäre: Server1.StoreDB.Sales.Order.

Beim Arbeiten in einer SQL Server-Datenbank ist es üblich, Tabellen und andere Objekte durch Hinzufügen des Schemanamens anzusprechen. So wird beispielsweise auf eine Tabelle im "Sales"-Schema als "Sales.tbl_Verkäufe" Bezug genommen, was deutlich macht, dass das Schema "Sales" ein eigener Bereich innerhalb der gleichen Datenbank ist und nicht mit dem Namen der Datenbank gleichgesetzt wird.

# SQL Anweisungen

SQL-Anweisungen werden an Hand ihrer Charakteristka gruppiert. Diese Gruppen sind:

| Gruppe | Beschreibung               | Beispiel                                           |
| ------ | -------------------------- | -------------------------------------------------- |
| DML    | Data Manipulation Language | Änderungsanweisungen wie INSERT, UPDATE und DELETE |
| DDL    | Data Definiton Language    | Anweisungen wie CREATE, ALTER und DROP             |
| DCL    | Data Control Language      | Anweisungen wie GRANT, REVOKE und DENY             |

In diesem Kurs konzentrieren wir uns auf Elemente der Abfragegruppe DML und ein wenig mit der Gruppe DDL, beispielsweise zum Erstellen von Sichten auf Daten. DCL ist eher administrativen Schulungen des SQL-Servers vorbehalten. 

