--
-- Nicht-ANSI-Funktion "ISNULL"
--
SELECT 
	ISNULL(Artikel, 'N/A') as Name
FROM [dbo].[tbl_Produkt]

--
-- Anwenden auf eine Spalte
--
SELECT 
	COALESCE(Artikel, 'N/A') as Name
FROM [dbo].[tbl_Produkt]

--
-- �bertragen der Funktion auf beliebig viele Spalten mit nur einem Befehl
--
SELECT 
	COALESCE(Artikel, WG1_Bezeichnung, WG2_Bezeichnung, 'N/A') as Name
FROM [dbo].[tbl_Produkt]

--
-- NULLIF
-- (Zeile 121)
-- 
SELECT 
      NULLIF(Umsatz, 0) AS Umsatz
FROM [dbo].[tbl_Verk�ufe]