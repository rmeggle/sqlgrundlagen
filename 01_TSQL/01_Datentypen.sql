--
-- Rechnen mit CAST
--
SELECT [Menge]
      ,[Umsatz]
	  ,CAST([Menge] / [Umsatz] AS Float)
  FROM [dbo].[tbl_Verk�ufe]
WHERE Mitarbeiter_ID=1

--
-- CAST 
--
SELECT 
      CAST([WG2_Bezeichnung] AS VarChar(50)) + ': ' + CAST([Artikel] AS varchar(10))
FROM [schulungsdatenbank].[dbo].[tbl_Produkt]


--
-- TRY_CAST
--
SELECT 
      TRY_CAST([WG2_Bezeichnung] AS VarChar(50)) + ': ' + TRY_CAST([Artikel] AS varchar(10))
FROM [schulungsdatenbank].[dbo].[tbl_Produkt]

--
-- CAST oder CONVERT?
--
SELECT 
	Zeit_ID 
FROM tbl_Geh�lter

--
-- Umwandeln von YYYY-MM-DD HH:MM:SS nach DD.MM.YYYY an Hand Codetabelle 104
--
SELECT 
	CONVERT(VARCHAR, Zeit_ID, 104) 
FROM tbl_Geh�lter

--
-- PARSE und TRY_PARSE
--
SELECT PARSE('1 January 2022' AS datetime USING 'de-de') AS Result;

--
-- STR
--
SELECT 
	STR(Umsatz) 
FROM tbl_Verk�ufe

-- Die implizite Konvertierung vom datetime-Datentyp in den float-Datentyp in der tbl_Geh�lter-Tabelle, Zeit_ID-Spalte ist nicht zul�ssig. Verwenden Sie die CONVERT-Funktion, um diese Abfrage auszuf�hren.
